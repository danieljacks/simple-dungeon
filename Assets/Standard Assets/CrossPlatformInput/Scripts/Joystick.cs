using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UnityStandardAssets.CrossPlatformInput
{
	public class Joystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
	{
		public enum AxisOption
		{
			// Options for which axes to use
			Both, // Use both
			OnlyHorizontal, // Only horizontal
			OnlyVertical // Only vertical
		}

		public int MovementRange = 100;
		public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use
		public string horizontalAxisName = "Horizontal"; // The name given to the horizontal axis for the cross platform input
		public string verticalAxisName = "Vertical"; // The name given to the vertical axis for the cross platform input


		Vector3 m_StartPos;
		bool m_UseX; // Toggle for using the x axis
		bool m_UseY; // Toggle for using the Y axis
		CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis; // Reference to the joystick in the cross platform input
		CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis; // Reference to the joystick in the cross platform input

		Vector3 m_PointerDownPos; // FIXED: Exact position of pointerdown so drag can be relative to that and not to pivot
        Vector3 m_MovementRange;

		public Transform restPos;

		void OnEnable()
		{
			CreateVirtualAxes();
		}

        void Start()
        {
            m_StartPos = transform.position;
 
            // FIXED: Calculate movement range in device units as scaled by canvas scaler
            CanvasScaler canvasScaler = GetComponentInParent<CanvasScaler> ();
            if (canvasScaler != null) {
                m_MovementRange = canvasScaler.transform.localScale * MovementRange;
				m_StartPos = new Vector3 (m_StartPos.x / canvasScaler.transform.localScale.x, m_StartPos.y / canvasScaler.transform.localScale.y, m_StartPos.z / canvasScaler.transform.localScale.z);
            }
            else {
                m_MovementRange = Vector3.one * MovementRange;
            }
        }

		void UpdateVirtualAxes(Vector3 value)
		{
			var delta = restPos.position - value;
            delta.y = -delta.y;
            if (m_UseX)
            {
                delta.x /= m_MovementRange.x; // FIXED: Use movement range in device units as scaled by canvas scaler
                m_HorizontalVirtualAxis.Update(-delta.x);
            }
 
            if (m_UseY)
            {
                delta.y /= m_MovementRange.y; // FIXED: Use movement range in device units as scaled by canvas scaler
                m_VerticalVirtualAxis.Update(delta.y);
            }
		}

		void CreateVirtualAxes()
		{
			// set axes to use
			m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
			m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

			// create new axes based on axes to use
			if (m_UseX)
			{
				m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);
				CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);
			}
			if (m_UseY)
			{
				m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
				CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);
			}
		}


		public void OnDrag(PointerEventData data)
		{
			float deltaX = 0;
            float deltaY = 0;
 
            if (m_UseX)
            {
                deltaX = Mathf.Clamp (data.position.x - m_PointerDownPos.x, -m_MovementRange.x, m_MovementRange.x); // FIXED: Take drag position relative to pointerdown position instead of pivot; Use movement range in device units as scaled by canvas scaler
            }
 
            if (m_UseY)
            {
                deltaY = Mathf.Clamp (data.position.y - m_PointerDownPos.y, -m_MovementRange.y, m_MovementRange.y); // FIXED: Take drag position relative to pointerdown position instead of pivot; Use movement range in device units as scaled by canvas scaler
            }
            transform.position = new Vector3(restPos.position.x + deltaX, restPos.position.y + deltaY, restPos.position.z);
            UpdateVirtualAxes(transform.position);
		}


		public void OnPointerUp(PointerEventData data)
		{
			transform.position = restPos.position;
			UpdateVirtualAxes(restPos.position);
		}


		public void OnPointerDown(PointerEventData data) {
			m_PointerDownPos = data.position;
		}

		void OnDisable()
		{
			// remove the joysticks from the cross platform input
			if (m_UseX)
			{
				m_HorizontalVirtualAxis.Remove();
			}
			if (m_UseY)
			{
				m_VerticalVirtualAxis.Remove();
			}
		}
	}
}