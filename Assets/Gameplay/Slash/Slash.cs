﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Attack))]
public class Slash : MonoBehaviour
{
    public GameObject slashLRPrefab;
    public GameObject slashRLPrefab;

    public static SlashDirection slashDirection = SlashDirection.LR;

    void Start () { // use then reverse slashDirection, and copy sender to instantiated slash
        var prefab = (slashDirection == SlashDirection.LR ? slashLRPrefab : slashRLPrefab);
        var obj = Instantiate (prefab, transform.position, transform.rotation, transform.parent) as GameObject;
        SwitchDirection ();

        obj.GetComponent<Attack> ().sender = GetComponent<Attack> ().sender;

        Destroy (gameObject);
    } 

    public static void SwitchDirection () {
        if (slashDirection == SlashDirection.LR) {
            slashDirection = SlashDirection.RL;
        } else {
            slashDirection = SlashDirection.LR;
        }
    }
}
