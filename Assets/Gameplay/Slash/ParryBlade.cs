﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerAttack))]
public class ParryBlade : MonoBehaviour {
    public float swipeTime = 0.5f;
    public float sameHandCooldown = 0.6f;
    public GameObject attackRLPrefab;
	public GameObject attackLRPrefab;
	public GameObject parrySuccessPrefabL;
	public GameObject parrySuccessPrefabR;
	public GameObject parryRLPrefab;
	public GameObject parryLRPrefab;

    private Task attackTask;
    private Hand prevAttackHand = Hand.None;
    private float prevAttackTime = 0;
    private bool canInterruptAttackRoutine = false;
    [SerializeField] [HideInInspector]
    private AttackArgs args;

    public void Left (AttackArgs newArgs) {
        RequestAttack (newArgs, Hand.Left);
    }

    public void Right (AttackArgs newArgs) {
        RequestAttack (newArgs, Hand.Right);
    }

    void RequestAttack (AttackArgs newArgs, Hand hand) {
        args = newArgs;
        StartAttackRoutineIfStopped (hand);
    }

    IEnumerator AttackRoutine (Hand hand) { // hand is what we use for the attack, args.currentHand is what is being pressed
        var attackPrefab = (args.currentHand == Hand.Left ? attackRLPrefab : attackLRPrefab);
        var attackPos = transform.position;
        var obj = Instantiate (attackPrefab, attackPos, Quaternion.identity, transform) as GameObject;

        prevAttackHand = args.currentHand; // this is before to always be what was actually instantiated
        
        while (obj != null) {
            yield return null;
        }
        
        prevAttackTime = Time.time; // this is after so the cooldown starts from attack end
    }

    void StartAttackRoutineIfStopped (Hand hand) {
        while (args.currentHand == prevAttackHand && Time.time - prevAttackTime < sameHandCooldown) {
            return;
        }

        if (attackTask == null || attackTask.Running == false) {
            attackTask = new Task (AttackRoutine (hand));
        }
    }
}
