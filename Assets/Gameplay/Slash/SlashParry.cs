﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class SlashParry : MonoBehaviour
{
    public float failParryCooldown = 2.0f;
    public GameObject parryLRPrefab;
    public GameObject parryRLPrefab;
    public GameObject parryLRFailPrefab;
    public GameObject parryRLFailPrefab;
    private Attack attack;

    void Start () {
        attack = GetComponent<Attack> ();
        if (attack.senderTag == "Player") {
            StartCoroutine (InstantiateRoutine ());
        } else { // from slasher
            var hand = attack.sender.GetComponent<SlasherAttack> ().newAttackHand;
            var slashDirection = (hand == Hand.Left ? SlashDirection.RL : SlashDirection.LR);
            GenerateParry (slashDirection);
        }
    }

    IEnumerator InstantiateRoutine () {
        yield return new WaitForFixedUpdate ();
        yield return new WaitForFixedUpdate ();
        foreach (var obj in GetComponent<TrackTriggerObjects> ().objects) {
            var slasherAttack   = obj.GetComponent<SlasherAttack> ();
            var enemy           = obj.GetComponent<Enemy> ();
            if (slasherAttack != null && enemy.CanBeParried () == true) {
                slasherAttack.AttemptParry ();
                GenerateParry (Slash.slashDirection);
                yield break;
            }
        }
        FailParry ();
    }

    void GenerateParry (SlashDirection direction) {
        var prefab = (direction == SlashDirection.LR ? parryLRPrefab : parryRLPrefab);
        var obj = Instantiate (prefab, transform.position, transform.rotation, transform.parent) as GameObject;
        Slash.SwitchDirection ();

        Destroy (gameObject);
    }

    void FailParry () {
        var prefab = (Slash.slashDirection == SlashDirection.LR ? parryLRFailPrefab : parryRLFailPrefab);
        var obj = Instantiate (prefab, transform.position, transform.rotation, transform.parent) as GameObject;
        Slash.SwitchDirection ();

        var playerAttack = attack.sender.GetComponent<PlayerAttack> ();
        playerAttack.SetCooldownAll (failParryCooldown);
        Destroy (gameObject);
    }

}
