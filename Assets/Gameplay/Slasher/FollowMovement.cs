﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ClosestFocus))]
public class FollowMovement : MonoBehaviour {

	public float closestDistance = 0.5f;
	public float farthestDistance = 1.0f;
	public float preferredDistancePercent = 0.75f; // 1 is farthest, 0 is closest
	public float moveSpeed = 2.0f;
	[HideInInspector]
	public float anchorRadius = 5; // Take this out if it is not being used
	public float followRadius = 8.0f;

	private ClosestFocus focus;
	private Vector2 targetPos;
	private Quaternion targetRot;
	private Vector2 startPos;
	private Task followTask = null;
	private Rigidbody2D rBody;

	void Start () {
		focus = GetComponent<ClosestFocus> ();
		rBody = GetComponent<Rigidbody2D> ();
		targetPos = transform.position;
		targetRot = transform.rotation;
		startPos = transform.position;

		followTask = new Task (FollowTarget ());
	}

	void Update () {
		if (followTask == null || followTask.Running == false) {
			if (!InRange() && TargetInFollowRadius()) {
				followTask = new Task (FollowTarget ());
			}
		}
	}

	private IEnumerator FollowTarget () {
		if (focus.shouldFocus == false) yield break;

		do {
			yield return null;
			if (this == null) yield break; // because the yield return null is before, the object can actually be destroyed in that frame
			
			if (!TargetInFollowRadius()) yield break;
				var dirFromTarget = ((Vector2) transform.position - focus.targetPosition).normalized;
				var unclippedPos = focus.targetPosition + dirFromTarget * Mathf.Lerp (closestDistance, farthestDistance, preferredDistancePercent);
				targetPos = GetClippedPos (unclippedPos);

			if (rBody == null) {
				transform.position = Vector2.MoveTowards ((Vector2) transform.position, targetPos, Time.deltaTime * moveSpeed);
			}
		} while ((Vector2) transform.position != targetPos);
	}

	Vector2 GetClippedPos (Vector2 target) {
		var distance = Vector2.Distance (transform.position, target);
		var direction = (target - (Vector2) transform.position).normalized;
		var mask = LayerMask.GetMask ("Terrain");
		RaycastHit2D hit = Physics2D.Raycast (transform.position, direction, distance, mask);
		if (hit.collider != null) {
			var col = GetComponent<Collider2D> ();
			var radius = Mathf.Max (col.bounds.extents.x, col.bounds.extents.y) * 1.3f;
			var hitOffset = ((Vector2) transform.position - hit.point).normalized * radius;
			return hit.point + hitOffset;
		} else {
			return target;
		}
	}

	void FixedUpdate () {
		if (rBody != null) {
			var nextPos = Vector2.MoveTowards ((Vector2) transform.position, targetPos, moveSpeed * Time.fixedDeltaTime);
			rBody.MovePosition (nextPos);
		}
	}

	private bool InRange () {
		float sqrMag = Vector2.SqrMagnitude((Vector2)transform.position - focus.targetPosition);
		// only start moving again if not in the correct range band
		if (sqrMag == Mathf.Clamp(sqrMag, closestDistance * closestDistance, farthestDistance * farthestDistance)) {
			return true;
		} else {
			return false;
		}
	}

	private bool TargetInAnchorRadius () {
		if (Vector2.SqrMagnitude(startPos - focus.targetPosition) <= anchorRadius * anchorRadius) {
			return true;
		} else {
			return false;
		}
	}

	private bool TargetInFollowRadius () {
		if (Vector2.SqrMagnitude((Vector2) transform.position - focus.targetPosition) <= followRadius * followRadius) {
			return true;
		} else {
			return false;
		}
	}
}
