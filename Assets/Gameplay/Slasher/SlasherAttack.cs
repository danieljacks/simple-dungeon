﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlasherAttack : MonoBehaviour {

	public float attackCooldownMin = 1.0f;
	public float attackCooldownMax = 2.0f;
	public float attackDistance = 2.0f;
	public Transform leftPosition;
	public Transform rightPosition;
	public GameObject attackLPrefab;
	public GameObject attackRPrefab;
	public GameObject parryLPrefab;
	public GameObject parryRPrefab;
	public GameObject warningLPrefab;
	public GameObject warningRPrefab;
	public float warningTime = 1.0f;
	public float sameHandDelay = 0.2f;
	public float sameHandChance = 0;


	private float lastAttackTime = 0f;
	private float startedWarningTime = 0f;
	private GameObject attackGO = null;
	private GameObject warningGO = null;
	private Enemy enemy;
	private Hand lastAttackHand = Hand.Left;
	private Hand nextAttackHand = Hand.Right;
	private bool waitExtraForSameHand = false;
	private float lastOutOfRange = 0;
	private LookAtFocus lookAtFocus;
	private Hand attemptParryHand = Hand.None;
	[HideInInspector] public Hand newAttackHand = Hand.Left;
	private Task behaviourTask = null;
	private System.Action onParryCallback = null;
	private bool wasParried = false;

	void Start () {
		enemy = GetComponent<Enemy> ();
		lookAtFocus = GetComponent<LookAtFocus> ();
	}

	void Update () {
		if (behaviourTask == null || !behaviourTask.Running) {
			if (TargetInRange ()) {
				behaviourTask = new Task (BehaviourRoutine (newAttackHand));
			}
		}
	}

	IEnumerator BehaviourRoutine (Hand hand) {
		lookAtFocus.enabled = false;
		enemy.isAttacking = true;

		enemy._canBeParried = true;
		yield return StartCoroutine (SignalRoutine (hand));
		enemy._canBeParried = false;

		if (wasParried == true) {
			wasParried = false;
			yield return StartCoroutine (ParryRoutine (hand));
		} else {
			yield return StartCoroutine (AttackRoutine (hand));
		}
		
		lookAtFocus.enabled = true;
		enemy.isAttacking = false;

		attemptParryHand = Hand.None;

		newAttackHand = GetNewAttackHand (hand);
		
		yield return new WaitForSeconds (GetCooldownTime (hand, newAttackHand));
	}

	IEnumerator SignalRoutine (Hand hand) {
		enemy.isWarning = true;

		var warningPos = hand == Hand.Left ? (Vector2) transform.position + Vector2.left : (Vector2) transform.position + Vector2.right;
		var warningPrefabUse = hand == Hand.Left ? warningLPrefab : warningRPrefab;
		warningGO = Instantiate (warningPrefabUse, warningPos, transform.rotation);
		var particle = warningGO.GetComponent<ParticleSystem> ();
		if (particle != null) {
			var main = particle.main;
			particle.Stop ();
			main.duration = warningTime;
			particle.Play ();
		}

		float startWaitTime = Time.time;
		while (Time.time < startWaitTime + warningTime) {
			if (wasParried == true) {
				break;
			}
			yield return null;
		}

		Destroy (warningGO);
		enemy.isWarning = false;
	}

	IEnumerator AttackRoutine (Hand hand) {
		var attackPos = hand == Hand.Left ? leftPosition.position : rightPosition.position;
		var attackPrefabToUse = hand == Hand.Left ? attackLPrefab : attackRPrefab;
		attackGO = Instantiate (attackPrefabToUse, attackPos, transform.rotation, transform);
		attackGO.GetComponent<Attack> ().senderTag = "Enemy";

		while (attackGO != null) {
			yield return null;
		}
	}

	IEnumerator ParryRoutine (Hand hand) {
		if (gameObject == null) yield break;

		if (onParryCallback != null) onParryCallback ();

		var parryPos = hand == Hand.Left ? leftPosition.position : rightPosition.position;
		var parryPrefabToUse = hand == Hand.Left ? parryLPrefab : parryRPrefab;
		attackGO = Instantiate (parryPrefabToUse, parryPos, transform.rotation, transform);

		var health = GetComponent<Health> ();
		if (health != null) {
			health.LoseHealth (1); //TODO: make this dependant on player attack
		}

		while (attackGO != null) {
			yield return null;
		}
	}

	bool CheckParryHand (Hand thisHand, Hand otherHand) {
		if (otherHand == thisHand) {
			return true;
		} else {
			return false;
		}
	}

	bool TargetInRange () {
		var targetObject = GameObject.FindGameObjectWithTag ("Player");
		if (targetObject != null && Vector2.Distance((Vector2) transform.position, (Vector2) targetObject.transform.position) <= attackDistance) {
			return true;
		} else {
			return false;
		}
	}

	float GetCooldownTime (Hand currentHand, Hand nextHand) {
		var attackCooldown = Random.Range (attackCooldownMin, attackCooldownMax);
		return attackCooldown + ((nextHand != Hand.None && nextHand != currentHand) ? sameHandDelay : 0);
	}

	public void AttemptParry () {
		if (enemy.CanBeParried()) {
			wasParried = true;
		}
	}

	Hand GetNewAttackHand (Hand currentAttackHand) {
		Hand hand;
		if (UnityEngine.Random.value >= sameHandChance) {
			hand = (currentAttackHand == Hand.Left ? Hand.Right : Hand.Left); // other hand
		} else {
			hand = (currentAttackHand == Hand.Left ? Hand.Left : Hand.Right); // same hand
			waitExtraForSameHand = true;
		}
		return hand;
	}
}
