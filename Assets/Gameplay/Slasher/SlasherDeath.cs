﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlasherDeath : MonoBehaviour, IDeath {

	public GameObject deathParticles;

	public void Die () {
		Instantiate (deathParticles, transform.position, Quaternion.identity);
		Destroy (gameObject);
	}
}
