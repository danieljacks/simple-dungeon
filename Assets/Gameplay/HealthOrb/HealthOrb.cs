﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthOrb : MonoBehaviour {
    public float healthGain = 5.0f;
    void OnTriggerEnter2D (Collider2D other) {
        if (other.CompareTag ("Player")) {
            var health = other.GetComponent<Health> ();
            if (health.health < health.maxHealth) {
                health.GainHealth (healthGain);
                Destroy (gameObject);
            }
        }
    }
}
