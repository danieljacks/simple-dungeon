﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : MonoBehaviour {

	public float dashDistance = 1.0f;
	public float dashDuration = 0.1f;
	public float dashCooldown = 2.0f;
	public GameObject dashReadyParticlePrefab;
	public GameObject dashEffectPrefab;
	public AnimationCurve dashCurve;
	public bool dashing = false;
	
	private Vector2 lastPos;

	private Task dashTask = null;
	private float dashStartTime = 0;
	private bool dashReady = true;
	private Vector2 dashDirection = Vector2.up;
	private Rigidbody2D rBody;

	void Start () {
		lastPos = transform.position;
		rBody = GetComponent<Rigidbody2D> ();
	}

	void Update () {
		Vector2 newDashDirection = ((Vector2) transform.position - lastPos).normalized;
		if (newDashDirection != Vector2.zero) {
			dashDirection = newDashDirection;
		}
		lastPos = transform.position;
	}

	public void StartDash () {
		if (dashTask == null || !dashTask.Running) {
			if (dashReady == true) {
				dashTask = new Task (DashRoutine ());
			}
		}
	}


	private IEnumerator DashRoutine () {
		dashing = true;
		dashReady = false;

		Vector2 startPos = transform.position;
		Vector2 endPos = GetClippedPos(startPos + (dashDirection * dashDistance));

		var health = GetComponent<Health> ();
		if (health) {
			health.SetInvincible (dashDuration * 1.3f);
		}

		var dashEffectObj = Instantiate (dashEffectPrefab, transform.position, dashEffectPrefab.transform.rotation, transform) as GameObject;
		var dashEffect = dashEffectObj.GetComponent<DashEffect> ();

		dashStartTime = Time.time;
		while (Time.time < dashStartTime + dashDuration) {
			float relTime = Time.time - dashStartTime;
			float dashSpeed = 1 / dashDuration;
			float t = dashCurve.Evaluate ((Time.time - dashStartTime) * dashSpeed);
			transform.position = Vector2.LerpUnclamped (startPos, endPos, t);
			yield return null;
		}
		transform.position = endPos; // make sure we reach the very end
		dashEffect.SetFinished ();
		dashEffect.transform.parent = null;

		dashing = false;
		StartCoroutine (DashCooldownRoutine());
	}

	private IEnumerator DashCooldownRoutine () {
		yield return new WaitForSeconds (dashCooldown);
		Instantiate (dashReadyParticlePrefab, transform.position, dashReadyParticlePrefab.transform.rotation);
		dashReady = true;
	}

	Vector2 GetClippedPos (Vector2 target) {
		var distance = Vector2.Distance (transform.position, target);
		var direction = (target - (Vector2) transform.position).normalized;
		var mask = LayerMask.GetMask ("Terrain");
		RaycastHit2D hit = Physics2D.Raycast (transform.position, direction, distance, mask);
		if (hit.collider != null) {
			var col = GetComponent<Collider2D> ();
			var radius = Mathf.Max (col.bounds.extents.x, col.bounds.extents.y) * 1.3f;
			var hitOffset = ((Vector2) transform.position - hit.point).normalized * radius;
			return hit.point + hitOffset;
		} else {
			return target;
		}
	}
}
