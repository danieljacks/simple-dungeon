﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class DashEffect : MonoBehaviour {

	private ParticleSystem pSystem;
	private bool canDestroy = false;

	void Start () {
		pSystem = GetComponent<ParticleSystem> ();
		pSystem.Clear ();
		pSystem.Stop ();
	}

	public void SetFinished () {
		pSystem.Play ();
		canDestroy = true;
	}

	void Update () {
		if(pSystem && canDestroy == true)
		{
			if(!pSystem.IsAlive())
			{
				Destroy(gameObject);
			}
		}
	}
}
