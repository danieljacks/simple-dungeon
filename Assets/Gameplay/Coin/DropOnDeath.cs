﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropOnDeath : MonoBehaviour, IDeath {
    public GameObject dropPrefab;
    public int minDrop = 1;
    public int maxDrop = 3;
    public float minForce = 2.0f;
    public float maxForce = 5.0f;
    public float dropDuration = 0.3f;
    public GameObject particlePrefab;

    public void Die () {
        new Task (DropRoutine (dropPrefab, transform.position));
        Instantiate (particlePrefab, transform.position, Quaternion.identity);
        Destroy (gameObject);
    }

    private IEnumerator DropRoutine (GameObject _dropPrefab, Vector3 spawnPos) { // drop prefab must be a parameter because this component is destroyed after the coroutine starts
        int drops = Random.Range (minDrop, maxDrop + 1);
        int alreadyDropped = 0;

        float startTime     = Time.time;
        float endTime       = startTime + dropDuration;

        float droppedPercent    = Mathf.InverseLerp (0, drops, alreadyDropped);
        float idealPercent      = Mathf.InverseLerp (startTime, endTime, Time.time);

        while (idealPercent < 1) {
            idealPercent = Mathf.InverseLerp (startTime, endTime, Time.time);
            int idealDropped = Mathf.RoundToInt (Mathf.Lerp (0, drops, idealPercent));
            int dropsThisFrame = idealDropped - alreadyDropped;

            for (int i = 0; i < dropsThisFrame; i++) { // create drops and give them a push
                var obj = Instantiate (_dropPrefab, spawnPos, Quaternion.identity) as GameObject;
                var objBody = obj.GetComponent<Rigidbody2D> ();
                
                if (objBody != null) {
                    var force = Random.Range (minForce, maxForce);
                    var direction = Random.insideUnitCircle.normalized;
                    objBody.AddForce (direction * force, ForceMode2D.Impulse);
                }
                alreadyDropped++;
            }

            yield return null;
        }

        
    }
}
