﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Coin : MonoBehaviour
{
    private bool addedCoins = false;

    void OnTriggerEnter2D (Collider2D other) {
        var playerCoins = other.GetComponent<PlayerCoins> ();
        if (playerCoins != null) {
            if (addedCoins == false) { // for some reason this function is called multiple times before the object is destroyed. This ensures coins are only added once
                playerCoins.AddCoins (1);
                addedCoins = true; 
            }
            Destroy (gameObject);
        }
    }
}
