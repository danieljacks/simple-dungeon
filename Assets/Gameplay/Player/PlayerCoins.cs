﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCoins : MonoBehaviour
{
    private Text coinsText;

    private int coins = 0;

    void Start () {
        coinsText = GameObject.FindGameObjectWithTag ("CoinsText").GetComponent<Text> ();
        SetCoinText ();
    }

    public void AddCoins (int coinsToAdd) {
        coins += coinsToAdd;
        SetCoinText ();
    }

    public void SubtractCoins (int coinsToSubtract) {
        coins -= coinsToSubtract;
        SetCoinText ();
    }

    private void SetCoinText () {
        coinsText.text = coins.ToString ();
    }
}
