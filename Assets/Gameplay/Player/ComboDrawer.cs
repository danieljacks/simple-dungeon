﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComboDrawer : MonoBehaviour
{
    public List<Image> slots = new List<Image> ();
    [SerializeField]
    public DictionaryComboElementSprite comboIcons = new DictionaryComboElementSprite ();

    private int currentSlot;
    private PlayerAttack playerAttack;

    void Start () {
        FindPlayerAttack ();

        OnComboReset ();
    }

    void Update () {
        if (playerAttack == null) {
            FindPlayerAttack ();
        }
    }

    void FindPlayerAttack () {
        var player = GameObject.FindGameObjectWithTag ("Player");
        if (player == null) return;
        
        playerAttack = player.GetComponent<PlayerAttack> ();
        if (playerAttack == null) return;

        playerAttack.OnComboAdded.RemoveListener (OnComboAdded);
        playerAttack.OnComboAdded.AddListener (OnComboAdded);
        playerAttack.OnComboReset.RemoveListener (OnComboReset);
        playerAttack.OnComboReset.AddListener (OnComboReset);
    }

    void OnComboAdded (ComboElement addedComboElement) {
        if (currentSlot > slots.Count - 1) return;
        Sprite newElementSprite = null;
        if (comboIcons.TryGetValue(addedComboElement, out newElementSprite)) { // only add the combo icon if it has an icon in the dictionary
            slots[currentSlot].sprite = newElementSprite;
            var color = slots[currentSlot].color;
            color.a = 1;
            slots[currentSlot].color = color;
            currentSlot++;
        }
    }

    void OnComboReset () {
        foreach (var slot in slots) {
            slot.sprite = null;
            var color = slot.color;
            color.a = 0;
            slot.color = color;
        }
        currentSlot = 0;
    }
}
