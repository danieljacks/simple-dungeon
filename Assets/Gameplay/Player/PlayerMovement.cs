﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(Dash))]
public class PlayerMovement : MonoBehaviour {

	public float speed = 4.0f;
	public float attackingSpeed = 2.0f;
	public bool useMobileInput = true;
	public PID2D movementPID = new PID2D (20, 0, 0.2f);
	
	private Animator animator;
	private Dash dash;
	private Rigidbody2D rBody;
	private Vector2 translate = Vector2.zero;
	private float useSpeed;

	void Start () {
		animator = GetComponent<Animator>();
		dash = GetComponent<Dash> ();
		rBody = GetComponent<Rigidbody2D> ();
		useSpeed = speed;
	}
	
	void Update () {
		translate = Vector2.zero;
		if (useMobileInput == true) {
			if (CrossPlatformInputManager.GetButton("arrowUp")) {
				dash.StartDash ();
			}
			if (dash.dashing == false) {
				translate = new Vector2 (CrossPlatformInputManager.GetAxis ("Horizontal"), CrossPlatformInputManager.GetAxis ("Vertical"));
			}
		} else {
			if (Input.GetKeyDown(KeyCode.Z)) {
				dash.StartDash ();
			}
			if (dash.dashing == false) {
				translate = new Vector2 (Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical"));
			}
		}

		var playerAttack = GetComponent<PlayerAttack> ();
		if (playerAttack != null && playerAttack.isAttacking == true) {
			useSpeed = attackingSpeed;
		} else {
			useSpeed = speed;
		}

		
		if (translate.SqrMagnitude () >= 0.0001f) {
			animator.SetBool ("walking", true);
		} else {
			animator.SetBool ("walking", false);
		}
	}

	void FixedUpdate () {
		if (rBody != null) {
			Vector2 goal = translate * useSpeed;
			rBody.AddForce(movementPID.Update (goal - rBody.velocity, Time.fixedDeltaTime));
		} else {
			transform.position += (Vector3) translate * useSpeed;
		}
	}
}
