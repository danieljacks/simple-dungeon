﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Events;
using System.Linq;

[System.Serializable] public class ComboAddedEvent : UnityEvent<ComboElement> {}
public class PlayerAttack : MonoBehaviour {
	public int comboMaxSize = 4;
	public List<Combo> combos = new List<Combo> ();
	public List<ComboElement> currentCombo = new List<ComboElement> ();
	public float noComboCooldown = 0.25f;

	[HideInInspector] public bool isAttacking = false;
	[HideInInspector] public UnityEvent<ComboElement> OnComboAdded = new ComboAddedEvent ();
	[HideInInspector] public UnityEvent OnComboReset = new UnityEvent ();

	private ComboElement lastComboElement = ComboElement.None;
	[HideInInspector] public float cooldownFinishedAll = 0;
	[HideInInspector] public float lastCooldownDuration = 0;
	[HideInInspector] private List<float> cooldownFinishedList = new List<float> (); 
	private Combo lastCombo = null;
	private Combo repeatCombo = null;

	void Start () {
		foreach (var combo in combos) {
			cooldownFinishedList.Add (0);
		}
	}

	void Update () {
		var comboInput = GetComboInput ();
		if (comboInput == lastComboElement && comboInput != ComboElement.Activate) { // only do anything the first time it is pressed, but activate can be held down
			return;
		}

		if (comboInput == ComboElement.Activate && repeatCombo == null && comboInput == lastComboElement) { // stops trying to activate constantly if there is no combo to activate
			return;
		}

		if (comboInput == ComboElement.None) return;

		switch (comboInput) {
			case ComboElement.Cancel:
				SetCooldownAll (noComboCooldown);
				ResetCombo ();
				break;
			case ComboElement.Activate:
				ActivateCombo ();
				break;
			default:
				AddComboElement (comboInput);
				break;
		}

		SetLastComboElement (comboInput);
	}

	void SetLastComboElement (ComboElement lastElement) {
		//if (lastElement == ComboElement.Activate && isOnCooldown(combos.IndexOf (GetCombo ()))) {
		//	return;
		//} else {
			lastComboElement = lastElement;
		//}
	}

	void ActivateCombo () {
		var combo = GetCombo ();
		
		if (isOnCooldown(combos.IndexOf (combo))) return;
		
		if (combo == null) {
			SetLastCombo (combo);
			SetCooldownAll (noComboCooldown);
			ResetCombo ();
			return;
		}

		if (combo.attackPrefab != null) {
			GameObject obj;
			if (combo.parentToPlayer == true) {
				obj = Instantiate (combo.attackPrefab, transform.position, transform.rotation, transform) as GameObject;
			} else {
				obj = Instantiate (combo.attackPrefab, transform.position, transform.rotation) as GameObject;
			}
			obj.GetComponent<Attack> ().sender = gameObject;
			obj.GetComponent<Attack> ().senderTag = tag;
		}

		SetLastCombo (combo);
		SetCooldown (combos.IndexOf(combo), combo.cooldown);
		SetCooldown (combos.IndexOf(repeatCombo), combos[combo.repeatCombo].cooldown);
		
		combo.comboEvent.Invoke ();
		
		ResetCombo ();
	}

	void SetLastCombo (Combo combo) {
		lastCombo = combo;
		if (combo == null) {
			repeatCombo = null;
			return;
		} else {
			repeatCombo = combos[combo.repeatCombo];
		}
	}

	Combo GetCombo () {
		if (currentCombo.Count == 0) {
			return repeatCombo;
		}

		foreach (var combo in combos) {
			bool comboMatches = true;

			if (currentCombo.Count != combo.elements.Count) comboMatches = false; // cant match if the combo has a different number of elements

			for (int i = 0; i < combo.elements.Count; i++) { // check for a combo match element-wise
				if (currentCombo[i] != combo.elements[i]) {
					comboMatches = false;
				}
			}

			if (comboMatches == true) {
				return combo;
			}
		}

		return null;
	}

	void AddComboElement (ComboElement comboElement) {
		if (currentCombo.Count >= comboMaxSize) return;
		if (currentCombo.Count > 0 && currentCombo.Last() == comboElement) return;

		currentCombo.Add (comboElement);
		OnComboAdded.Invoke (comboElement);
	}

	void ResetCombo () {
		OnComboReset.Invoke ();
		currentCombo.Clear ();
	}

	public void SetCooldown (int comboIndex, float duration) {
		lastCooldownDuration = duration;
		cooldownFinishedList[comboIndex] = Time.time + duration;
	}

	public void SetCooldownAll (float duration) {
		lastCooldownDuration = duration;
		cooldownFinishedAll = Time.time + duration;
	}

	bool isOnCooldown (int comboIndex) {
		if (comboIndex == -1) {
			if (Time.time > cooldownFinishedAll) {
				return false;
			} else {
				return true;
			}
		}
		if (Time.time > cooldownFinishedAll && Time.time > cooldownFinishedList[comboIndex]) {
			return false;
		} else {
			return true;
		}
	}

	public void SetLastCooldown (float duration) {
		SetCooldown (combos.IndexOf (lastCombo), duration);
	}

	ComboElement GetComboInput () {
		if (CrossPlatformInputManager.GetButton("comboLeft") == true ) {
			return ComboElement.Left;
		} else if (CrossPlatformInputManager.GetButton("comboRight") == true) {
			return ComboElement.Right;
		} else if (CrossPlatformInputManager.GetButton("comboUp") == true) {
			return ComboElement.Up;
		} else if (CrossPlatformInputManager.GetButton("comboDown") == true) {
			return ComboElement.Down;
		} else if (CrossPlatformInputManager.GetButton("comboActivate") == true) {
			return ComboElement.Activate;
		} else if (CrossPlatformInputManager.GetButton("comboCancel") == true) {
			return ComboElement.Cancel;
		} else {
			return ComboElement.None;
		}
	}
}