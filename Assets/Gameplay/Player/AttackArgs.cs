﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AttackArgs {
    public Hand currentHand;
    public Hand lastHand;
    public float swipeDuration;
    public float lastNoHandDuration;
    public bool isSwipe;
    public System.Action attackStartCallback;
    public System.Action attackFinishedCallback;

    public AttackArgs (Hand _currentHand, Hand _lastHand, float _swipeDuration, bool _isSwipe, float _lastNoHandDuration, System.Action _attackStartCallback, System.Action _attackFinishedCallback) {
        currentHand = _currentHand;
        lastHand = _lastHand;
        swipeDuration = _swipeDuration;
        isSwipe = _isSwipe;
        lastNoHandDuration = _lastNoHandDuration;
        attackStartCallback = _attackStartCallback;
        attackFinishedCallback = _attackFinishedCallback;
    }

    
}
