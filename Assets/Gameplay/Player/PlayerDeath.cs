﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerDeath : MonoBehaviour, IDeath {

	public void Die () {
		GetComponent<Health> ().health = GetComponent<Health> ().maxHealth;
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);
	}
	
}
