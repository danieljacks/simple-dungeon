﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(TrackCollisionObjects))]
public class Portal : MonoBehaviour {
    public float activateTime = 1.5f;
    public GameObject highlightObject;
    private Scene destinationScene;
    private float progress = 0;
    private TrackCollisionObjects tracker;
    private Vector3 highlightInitialScale;

    void Start () {
        tracker = GetComponent<TrackCollisionObjects> ();
        destinationScene = SceneManager.GetActiveScene ();
        highlightInitialScale = highlightObject.transform.localScale;
    }

    void Update() {
        var newScale = highlightInitialScale;
        newScale.y *= progress;
        highlightObject.transform.localScale = newScale;
        
        if (progress >= 1) {
            ActivatePortal ();
        }

        if (tracker.objects.Count > 0) { // player is touching portal
            progress += (1 / activateTime) * Time.deltaTime;
        } else { // player not touching portal
            progress -= (1 / activateTime) * Time.deltaTime;
        }

        progress = Mathf.Clamp01 (progress);
    }

    void ActivatePortal () {
        SceneManager.LoadScene (destinationScene.buildIndex);
    }
}
