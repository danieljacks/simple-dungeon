﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeDeath : MonoBehaviour, IDeath {

	float separationForce = 1.0f;

	public void Die () {
		var parent = GetComponent<Slime> ();

		if (parent.slimeSize <= 1) {
			Destroy (gameObject);
			return;
		}

		float angle = Random.Range (0f, 2f * Mathf.PI);
		float radius = Random.Range (0.5f, 1f);
		Vector2 direction = new Vector2 (Mathf.Cos (angle), Mathf.Sin (angle));
		direction = direction * radius * ((float) parent.slimeSize / (float) parent.slimeMaxSize);
		var slime1 = Instantiate (EnemyTypeManager.Instance.slimePrefab, (Vector2) parent.transform.position + direction, Quaternion.identity) as GameObject;
		var slime2 = Instantiate (EnemyTypeManager.Instance.slimePrefab, (Vector2) parent.transform.position - direction, Quaternion.identity) as GameObject;
		
		slime1.GetComponent<Rigidbody2D> ().AddForce (direction * separationForce, ForceMode2D.Impulse);
		slime2.GetComponent<Rigidbody2D> ().AddForce (-direction * separationForce, ForceMode2D.Impulse);

		slime1.GetComponent<Slime> ().slimeSize = parent.slimeSize - 1;
		slime2.GetComponent<Slime> ().slimeSize = parent.slimeSize - 1;

		// bit of a hack to stop the baby slimes getting hit with the same slash as the parent slime
		foreach (var go in GameObject.FindGameObjectsWithTag("Attack")) {
			var attack = go.GetComponent<Attack> ();
			if (attack.hitTargets.Contains (parent.gameObject)) {
				attack.hitTargets.Add (slime1);
				attack.hitTargets.Add (slime2);
			}
		}

		Destroy (gameObject);
	}
}
