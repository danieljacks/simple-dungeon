﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Focus))]
public class SlimeAttack : MonoBehaviour {

	public float attackCooldown = 2.0f;
	public float warnTime = 0.5f;
	public GameObject attackPrefab;
	public GameObject warningPrefab;
	public float attackRadius = 10.0f;
	
	private float lastAttackTime;
	private GameObject attackObject = null;
	private Focus focus;

	void Start () {
		focus = GetComponent<Focus> ();
		lastAttackTime = Time.time;
		StartCoroutine (Warn());
	}


	IEnumerator Warn () {
		while (Vector2.SqrMagnitude ((Vector2) transform.position - focus.targetPosition) > attackRadius * attackRadius) {
			yield return null;
		}

		var obj = Instantiate (warningPrefab, transform.position, transform.rotation, transform) as GameObject;
		yield return new WaitForSeconds (warnTime);
		Destroy (obj);
		StartCoroutine (Attack ());
	}

	IEnumerator Attack () {
		attackObject = Instantiate (attackPrefab, transform.position, transform.rotation) as GameObject;
		attackObject.transform.localScale = transform.localScale;
		attackObject.GetComponent<Attack> ().damageMultiplier = transform.lossyScale.x;
		while (attackObject != null) {
			yield return null;
		}
		yield return new WaitForSeconds (attackCooldown);
		StartCoroutine (Warn ());
	}
}
