﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Entity))]
public class Slime : Enemy {

	public int slimeMaxSize = 3;
	public int slimeSize = 3;
	public float slimeRatio;
	public GameObject slimePrefab;

	public void Start () {
		int levelsDown = slimeMaxSize - slimeSize;
		slimeRatio = 1 / Mathf.Pow (2f, (float) levelsDown);
		slimeRatio = Mathf.Round (slimeRatio * 100f) / 100f;
		transform.localScale *= slimeRatio;
		var health = GetComponent<Health> ();
		health.maxHealth = Mathf.Round (health.maxHealth * slimeRatio);
		health.health = Mathf.Round (health.health * slimeRatio);
		var entity = GetComponent<Entity> ();
		entity.size *= slimeRatio;
	}

}
