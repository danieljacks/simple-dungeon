﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Combo {
    public List<ComboElement> elements = new List<ComboElement> ();
    public float cooldown = 1.0f;
    public GameObject attackPrefab;
    public UnityEvent comboEvent;
    public int repeatCombo;
    public bool parentToPlayer = true;
}
