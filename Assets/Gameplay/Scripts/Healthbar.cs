﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Health))]
public class Healthbar : MonoBehaviour {

	public Slider healthSlider;
	private Health health;

	void Start () {
		health = GetComponent<Health> ();
	}

	void Update () {

		healthSlider.value = health.health / health.maxHealth;
	}
}
