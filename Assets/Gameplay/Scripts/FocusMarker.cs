﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Focus))]
public class FocusMarker : MonoBehaviour {

	public GameObject markerPrefab;
	private GameObject markerObject = null;
	private Focus focus;

	void Start () {
		focus = GetComponent<Focus> ();
	}
	
	void LateUpdate () {
		if (markerObject == null && focus.shouldFocus == true) {
			markerObject = Instantiate (markerPrefab, focus.targetPosition, markerPrefab.transform.rotation) as GameObject;
		} 
		if (markerObject != null && focus.shouldFocus == false) {
			Destroy (markerObject);
		}

		MoveAimingObject ();
	}

	void MoveAimingObject () {
		if (markerObject == null) return;
		if (focus.targetObject != null) {
			float targetSize = focus.targetObject.GetComponent<Entity> ().size;
			markerObject.transform.localScale = new Vector3 (1, 1, 1) * targetSize * 1.5f;
		} else {
			markerObject.transform.localScale = new Vector3 (1, 1, 1); // just normal scale if not snapped to anything
		}

		markerObject.transform.position = focus.targetPosition;
	}
}
