﻿using System.Collections;
[System.Serializable]
public enum SlashDirection {
    LR,
    RL
}