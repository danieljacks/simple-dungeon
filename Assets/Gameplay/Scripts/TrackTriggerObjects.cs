﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// Keeps a list of all objects colliding with a trigger
[RequireComponent(typeof(Collider2D))]
public class TrackTriggerObjects : MonoBehaviour {

	public string otherTag = "";

	private List<GameObject> _objects = new List<GameObject> ();
	public List<GameObject> objects {
		get {
			VerifyObjects ();
			return _objects;
		}
		set {
			_objects = value;
		}
	}

	public bool CheckTriggering (GameObject obj) {
		return _objects.Contains (obj);
	}

	private void VerifyObjects () {
		for (int i = _objects.Count - 1; i >= 0; i--) {
			if (_objects[i] == null || _objects[i].activeInHierarchy == false) {
				_objects.RemoveAt(i);
			}
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (otherTag == "" || other.CompareTag(otherTag)) {
			if (!_objects.Contains(other.gameObject)) {
				_objects.Add (other.gameObject);
			}
		}
	}

	void OnTriggerExit2D (Collider2D other) {
		_objects.Remove(other.gameObject);
	} 
}
