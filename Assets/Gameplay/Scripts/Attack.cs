﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour {

	public enum DamageType {
		Single,
		DOT,
	}

	public string senderTag;
	public GameObject sender;
	public List<DOT> DOTs = new List<DOT> ();
	public float damage = 1.0f;
	public float damageMultiplier = 1.0f;

	[HideInInspector]
	public List<GameObject> hitTargets = new List<GameObject> ();

	void OnTriggerEnter2D (Collider2D other) {
		if (!other.CompareTag(senderTag)) {
			var health = other.GetComponent<Health> ();
			if (health != null) {
				ApplyAttack (health);
			}
		}
	}

	void OnParticleCollision (GameObject other) {
		var col = other.GetComponent<Collider2D> ();
		if (col != null) {
			OnTriggerEnter2D (col);
		}	
	}

	void ApplyAttack (Health health) {
		if (!hitTargets.Contains (health.gameObject)) {
			hitTargets.Add (health.gameObject);
			health.LoseHealth (damage * damageMultiplier);
		}

		foreach (DOT dot in DOTs) {
			health.ApplyDOT (dot);
		}
	}
}
