﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockRotation : MonoBehaviour {

	public float originalRotation;

	void LateUpdate () {
		transform.rotation = Quaternion.Euler (0, 0, originalRotation);
	}
}
