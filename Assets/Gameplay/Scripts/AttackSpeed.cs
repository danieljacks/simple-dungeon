﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AttackSpeed : MonoBehaviour
{
    
    public float attackSpeed = 1.0f;

    void Start() {
        GetComponent<Animator> ().SetFloat ("attackSpeed", attackSpeed);
        var trailRenderer = GetComponent<TrailRenderer> ();
        if (trailRenderer != null) {
            trailRenderer.time /= attackSpeed;
        }
    }
}
