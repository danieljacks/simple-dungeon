﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Focus))]
public class Attract : MonoBehaviour
{
    public float radius = 5.0f;
    public float travelTime = 0.5f;

    private Focus focus;
    private Vector2 velocity;

    void Start () {
        focus = GetComponent<Focus> ();
    }

    void Update () {
        if (Vector2.SqrMagnitude ((Vector2) transform.position - focus.targetPosition) <= radius * radius) {
            transform.position = Vector2.SmoothDamp (transform.position, focus.targetPosition, ref velocity, travelTime, Mathf.Infinity, Time.deltaTime);
        }
    }
}
