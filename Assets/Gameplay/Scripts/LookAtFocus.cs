﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Focus))]
public class LookAtFocus : MonoBehaviour {

	public float rotateSpeed = 6.0f;
	public float range = 4.0f;
	private Rigidbody2D rBody;
	private Focus focus;

	void Start () {
		rBody = GetComponent<Rigidbody2D> ();
		focus = GetComponent<Focus> ();
	}

	// if you ever change this to not use the Update function, it may break the bit in the attack script referencing this.enabled
	void Update () {
		if (rBody != null) return;
		if (!IsInRange ()) return;

		var dir = focus.targetPosition - (Vector2) transform.position;
		var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
		angle = Mathf.MoveTowardsAngle (angle, angle - 90, Mathf.Infinity);
		Quaternion targetRotation = Quaternion.AngleAxis(angle, Vector3.forward);

		transform.rotation = Quaternion.Slerp (transform.rotation, targetRotation, Time.deltaTime * rotateSpeed);
	}

	public PID angularVelocityController = new PID(5, 0, 0.4f);
	public PID headingController = new PID(6, 0, 0.2f);

	public void FixedUpdate ()
	{
		if (rBody == null) return;

		var angularVelocityError = rBody.angularVelocity * -1 * Mathf.Deg2Rad;
		//Debug.DrawRay(transform.position, rBody.angularVelocity * 10, Color.black);
		
		var angularVelocityCorrection = angularVelocityController.Update(angularVelocityError, Time.fixedDeltaTime);
		//Debug.DrawRay(transform.position, angularVelocityCorrection, Color.green);

		rBody.AddTorque(angularVelocityCorrection * 0.01f);

		if (!IsInRange ()) return;
		
		var desiredHeading = focus.targetPosition - (Vector2) transform.position;
		//Debug.DrawRay(transform.position, desiredHeading, Color.magenta);

		var currentHeading = (Vector2) transform.up;
		Debug.DrawRay(transform.position, currentHeading * 15, Color.black);

		var headingError = Vector2.SignedAngle(currentHeading, desiredHeading) * Mathf.Deg2Rad;

		var headingCorrection = headingController.Update(headingError, Time.deltaTime);

		rBody.AddTorque(headingCorrection * 0.1f);
	}

	bool IsInRange () {
		var sqrRange = range * range;
		if (Vector2.SqrMagnitude (focus.targetPosition - (Vector2) transform.position) <= sqrRange) {
			return true;
		} else {
			return false;
		}
	}
}
