﻿[System.Serializable]
public enum ComboElement {
    Left,
    Right,
    Up,
    Down,
    Activate,
    Cancel,
    None
}