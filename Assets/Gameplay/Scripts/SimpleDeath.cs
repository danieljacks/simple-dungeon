﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleDeath : MonoBehaviour, IDeath {

	public void Die () {
		Destroy (gameObject);
	}
}
