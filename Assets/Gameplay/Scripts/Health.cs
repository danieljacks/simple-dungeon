﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(IDeath))]
public class Health : MonoBehaviour {
	public float health;
	public float maxHealth = 5f;
	public Dictionary<int, DOT> DOTs = new Dictionary<int, DOT> ();


	private bool invincible = false;
	private float invincibleEnd = 0;
	private Task invincibleTask = null;

	void Start () {
		health = maxHealth;
	}

	public void LoseHealth (float amount) {
		if (invincible) return;

		health -= amount;
		if (health <= 0) {
			health = 0;
			foreach (var death in GetComponents<IDeath>()) {
				if (gameObject != null) {
					death.Die ();
				}
			}
		}
	}

	public void GainHealth (float amount) {
		health += amount;
		if (health >= maxHealth) {
			health = maxHealth;
		}
	}

	public void ApplyDOT (DOT dot) {
		if (invincible) return;

		if (DOTs.ContainsKey(dot.id)) {
			DOTs[dot.id].RefreshDOT ();
		} else {
			DOTs.Add (dot.id, dot);
			dot.health = this;
			dot.StartDOT ();
		}
	}

	public void SetInvincible (float duration) {
		if (invincibleEnd < Time.time + duration) {
			invincibleEnd = Time.time + duration;
		}
		if (invincibleTask == null || !invincibleTask.Running) {
			invincibleTask = new Task (InvincibleRoutine());
		}
	}

	private IEnumerator InvincibleRoutine () {
		invincible = true;
		while (Time.time < invincibleEnd) {
			yield return null;
		}
		invincible = false;
	}

	
}
