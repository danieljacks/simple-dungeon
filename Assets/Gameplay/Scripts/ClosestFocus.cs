﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosestFocus : Focus {

	[HideInInspector]
	public float distanceToTarget = Mathf.Infinity;
	public string focusTag = "";
	

	void Update () {
		distanceToTarget = Mathf.Infinity;
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag (focusTag)) {
			float mag = ((Vector2) obj.transform.position - (Vector2) transform.position).magnitude;
			if (mag < distanceToTarget) {
				distanceToTarget = mag;
				targetObject = obj;
			}
		}

		if (targetObject != null) {
			if (shouldUpdateFocusPos == true) {
				targetPosition = targetObject.transform.position;
			}
			shouldFocus = true;
		} else {
			shouldFocus = false;
		}
	}


}
