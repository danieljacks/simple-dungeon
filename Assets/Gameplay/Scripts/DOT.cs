﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DOT {
	[HideInInspector]
	public Health health;
	public float damage;
	public float duration;
	public float interval;

	private bool isRunning = false;
	private float appliedDamage = 0;
	private float startTime;
	private Coroutine Routine = null;
	private static int nextID = 0;
	[HideInInspector]
	public int id = 0;

	public DOT (float _damage, float _duration, float _interval) {
		damage = _damage;
		duration = _duration;
		interval = _interval;

		startTime = Time.time;
	}

	public void StartDOT () {
		id = nextID;
		nextID += 1;

		if (Routine != null) {
			health.StopCoroutine (Routine);
		}
		Routine = health.StartCoroutine (Tick());
	}

	public void RefreshDOT () {
		appliedDamage = 0;
		startTime = Time.time;
		if (isRunning == false) {
			Routine = health.StartCoroutine (Tick());
		}
	}

	private IEnumerator Tick () {
		isRunning = true;
		appliedDamage = 0;
		startTime = Time.time;

		var spriteRenderer = health.gameObject.GetComponent<SpriteRenderer> ();
		var oldColor = Color.white;
		if (spriteRenderer != null) {
			oldColor = spriteRenderer.color;
			spriteRenderer.color = Color.green;
		}

		while (appliedDamage < damage) {
			yield return new WaitForSeconds (interval);
			float t = Time.time - startTime;
			float idealDamage = (damage / duration) * interval * Mathf.Round (t / interval);
			if (idealDamage > damage) idealDamage = damage;
			health.LoseHealth (idealDamage - appliedDamage);
			appliedDamage = idealDamage;
		}

		spriteRenderer = health.gameObject.GetComponent<SpriteRenderer> ();
		if (spriteRenderer != null) {
			spriteRenderer.color = oldColor;
		}

		isRunning = false;
	}
}
