﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Focus : MonoBehaviour{
	[HideInInspector]
	public Vector2 targetPosition = Vector2.zero;
	[HideInInspector]
	public bool shouldFocus = true;
	[HideInInspector]
	public bool shouldUpdateFocusPos = true;
	[HideInInspector]
	public GameObject targetObject = null;
}
