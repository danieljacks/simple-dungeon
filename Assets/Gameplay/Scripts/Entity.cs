﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour {

	public int ID;
	public float size = 1.0f;
	public static int lastID = -1;
	public static int GetNewID () {
		lastID++;
		return lastID;
	}

	void Awake () {
		ChangeID ();
	}

	public void ChangeID () {
		ID = GetNewID ();
	}
}
