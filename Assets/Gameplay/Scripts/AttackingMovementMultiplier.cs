﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
[RequireComponent(typeof(FollowMovement))]
public class AttackingMovementMultiplier : MonoBehaviour {
    
    public float multiplier = 1.0f;

    private float standardMovementSpeed;
    private Enemy enemy;
    private FollowMovement movement;

    void Start () {
        enemy = GetComponent<Enemy> ();
        movement = GetComponent<FollowMovement> ();

        standardMovementSpeed = movement.moveSpeed;
    }

    void Update () {
        if (enemy.isAttacking == true) {
            movement.moveSpeed = standardMovementSpeed * multiplier;
        } else {
            movement.moveSpeed = standardMovementSpeed;
        }
    }
}
