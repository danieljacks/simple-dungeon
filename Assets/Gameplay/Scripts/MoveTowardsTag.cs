﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowardsTag : MonoBehaviour {

	public string targetTag;
	public float speed = 3.0f;
	public bool adjustDirection = false;

	private Vector2 direction;
	private Transform target;
	private Vector3 lastTargetPosition = Vector2.zero;

	public void Start () {
		target = GameObject.FindGameObjectWithTag (targetTag).transform;
		if (target != null) {
			direction = (target.position - transform.position).normalized;
		} else {
			direction = Random.insideUnitCircle;
		}
	}

	public void Update () {
		if (adjustDirection == true) {
			if (target != null) {
				transform.position = Vector2.MoveTowards (transform.position, target.position, speed * Time.deltaTime);
			} else {
				transform.position = Vector2.MoveTowards (transform.position, lastTargetPosition, speed * Time.deltaTime);
			}
		} else {
			transform.position += (Vector3) direction * speed * Time.deltaTime;
		}

		lastTargetPosition = target.position;
	}
}
