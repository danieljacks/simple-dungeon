﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseFocus : Focus {
	
	public float snapThreshold = 2.0f;
	public string focusTag = "";

	private List<GameObject> objectsWithTag = new List<GameObject> ();
	private float targetToMouseDistance = Mathf.Infinity;

	void Update () {
		targetToMouseDistance = Mathf.Infinity;
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag (focusTag)) {
			float mag = ((Vector2) obj.transform.position - (Vector2) Camera.main.ScreenToWorldPoint(Input.mousePosition)).magnitude;
			if (mag < targetToMouseDistance) {
				targetToMouseDistance = mag;
				targetObject = obj;
			}
		}

		SetFocusPos ();
	}

	void SetFocusPos () {
		if (targetToMouseDistance < snapThreshold) { // this means the aim will only snap if within a threshold, otherwise just follow the mouse
			targetPosition = targetObject.transform.position;
		} else {
			targetPosition = (Vector2) Camera.main.ScreenToWorldPoint (Input.mousePosition);
		}
	}
}
