﻿using UnityEngine;

[System.Serializable]
public class PID2D
 {
     [SerializeField]
     public float pFactor, iFactor, dFactor;
 
     private Vector2 integral;
     private Vector2 lastError;
 
     public PID2D(float pFactor, float iFactor, float dFactor)
     {
         this.pFactor = pFactor;
         this.iFactor = iFactor;
         this.dFactor = dFactor;
     }
 
     public Vector2 Update(Vector2 currentError, float timeFrame)
     {
         integral += currentError * timeFrame;
         var deriv = (currentError - lastError) / timeFrame;
         lastError = currentError;
         return currentError * pFactor
             + integral * iFactor
             + deriv * dFactor;
     }
 }
 