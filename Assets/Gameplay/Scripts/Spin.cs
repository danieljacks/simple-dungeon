﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour
{
    public float spinPeriod = 1.0f;

    public void Update () {
        var desiredRotation = (360 * Time.time) / spinPeriod;
        transform.rotation = Quaternion.Euler (0, 0, desiredRotation);
    }
}
