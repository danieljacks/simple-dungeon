﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Entity))]
[RequireComponent(typeof(IDeath))]
[RequireComponent(typeof(Health))]
public class Enemy : MonoBehaviour {
    [HideInInspector]
    public bool isWarning = false;
    [HideInInspector]
    public bool _canBeParried = false;
    [HideInInspector]
    public Hand _canParryHand = Hand.Left;
    [HideInInspector]
    public bool isAttacking = false;
    public virtual bool CanBeParried () {
        return _canBeParried;
    }
}
