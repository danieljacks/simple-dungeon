﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CatchCo;

// TODO: implement the ability to skip a certain number of frames between each redraw for speed optimization
[RequireComponent(typeof(Image))]
public class Minimap : MonoBehaviour {
    public Color emptyColor = Color.cyan;
    public Color wallColor = Color.grey;
    public Color unexploredColor = Color.black;
    public Color playerColor = Color.green;
    public int playerRadius = 2;
    public Color portalColor = Color.magenta;
    public int portalRadius = 2;
    public int redrawFrequency = 4;

    [SerializeField]
    private bool[,] rememberedCells;
    private RectInt visibleGridBounds;
    private int frameCount = 0;


    void Start () {
        Reset ();
    }

    void Update () {
        if (rememberedCells == null) Reset ();

        Vector3 worldPos = Camera.main.ScreenToWorldPoint (new Vector3 ((float) Screen.width / (float) 2, (float) Screen.height / (float) 2, 0));
        Vector2Int boundsCenter = GridManager.Instance.WorldToGridPos(worldPos);
        var horzSize = Camera.main.orthographicSize * 2 * Screen.width / Screen.height;
        var vertSize = Camera.main.orthographicSize * 2;

        Vector2Int boundsSize = new Vector2Int (Mathf.CeilToInt(horzSize / GridManager.Instance.cellSize), Mathf.CeilToInt(vertSize / GridManager.Instance.cellSize));
        Vector2Int boundsExtents = new Vector2Int (Mathf.CeilToInt (horzSize / (GridManager.Instance.cellSize * 2)), Mathf.CeilToInt (vertSize / (GridManager.Instance.cellSize * 2)));
        visibleGridBounds = new RectInt (boundsCenter - boundsExtents, boundsSize);

        foreach (var pos in visibleGridBounds.allPositionsWithin) {
            rememberedCells[pos.x, pos.y] = true;
        }

        if (frameCount % redrawFrequency == 0) { // redraw every nth frame, where n is redrawFrequency
            Redraw ();
        }

        frameCount++;
    }

    [ExposeMethodInEditor]
    public void RevealAll () {
        var width = GridManager.Instance.gridWidth;
        var height = GridManager.Instance.gridHeight;
        //var texture = new Texture2D (width, height);

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                rememberedCells[x, y] = true;
            }
        }
    }

    void Redraw () {
        var width = GridManager.Instance.gridWidth;
        var height = GridManager.Instance.gridHeight;
        //var texture = new Texture2D (width, height);

        var pixels = new Color[width * height];
        int i = 0;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (rememberedCells[x, y] == true) {
                    pixels[i] = GridManager.Instance.GetCell (new Vector2Int(x, y)) == CellType.Empty ? emptyColor : wallColor;
                } else {
                    pixels[i] = unexploredColor;
                }
                i++;
            }
        }
        
        var image = GetComponent<Image> ();
        image.sprite.texture.filterMode = FilterMode.Point;
        image.sprite.texture.SetPixels (pixels);

        var player = GameObject.FindGameObjectWithTag ("Player");
        if (player != null) {
            var gridPos = GridManager.Instance.WorldToGridPos (player.transform.position);
            for (int y = 0; y < playerRadius * 2 - 1; y++) {
                for (int x = 0; x < playerRadius * 2 - 1; x++) {
                    image.sprite.texture.SetPixel (gridPos.x - playerRadius + x + 1, gridPos.y - playerRadius + y + 1, playerColor);
                }
            }
        }

        var portal = GameObject.FindGameObjectWithTag ("Portal");
        if (portal != null) {
            var gridPos = GridManager.Instance.WorldToGridPos (portal.transform.position);
            if (rememberedCells[gridPos.x, gridPos.y] == true) {
                for (int y = 0; y < portalRadius * 2 - 1; y++) {
                    for (int x = 0; x < portalRadius * 2 - 1; x++) {
                        image.sprite.texture.SetPixel (gridPos.x - portalRadius + x + 1, gridPos.y - portalRadius + y + 1, portalColor);
                    }
                }
            }
        }

        image.sprite.texture.Apply ();
    }

    void Reset () {
        var width   = GridManager.Instance.gridWidth;
        var height  = GridManager.Instance.gridHeight;

        rememberedCells = new bool[width, height];
        visibleGridBounds = new RectInt (0, 0, 0, 0);

        GetComponent<Image> ().sprite = Sprite.Create (new Texture2D (width, height, TextureFormat.RGBA32, false), new Rect (0, 0, width, height), new Vector2 (0.5f, 0.5f));
    }
}
