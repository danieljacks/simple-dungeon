﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeIndicator : MonoBehaviour {
    public ComboElement comboElement;
    public float addDuration = 1.0f;
    public AnimationCurve addCurve;
    public float resetDuration = 0.5f;
    public AnimationCurve resetCurve;

    private Vector3 scaleStart;
    private PlayerAttack playerAttack;
    private Task animationTask;

    private enum DurationType {
        Add,
        Cooldown
    }

    void Start () {
        scaleStart = transform.localScale;
        transform.localScale = scaleStart * 0;
        FindPlayerAttack ();
    }

    void Update () {
        if (playerAttack == null) {
            FindPlayerAttack ();
        }
    }

    void FindPlayerAttack () {
        var player = GameObject.FindGameObjectWithTag ("Player");
        if (player == null) return;
        
        playerAttack = player.GetComponent<PlayerAttack> ();
        if (playerAttack == null) return;

        playerAttack.OnComboAdded.RemoveListener (OnComboAdded);
        playerAttack.OnComboAdded.AddListener (OnComboAdded);
        playerAttack.OnComboReset.RemoveListener (OnComboReset);
        playerAttack.OnComboReset.AddListener (OnComboReset);
    }

    void OnComboAdded (ComboElement addedElement) {
        if (addedElement != comboElement) return;

        if (animationTask != null) animationTask.Stop (); // stop if running

        animationTask = new Task (AnimationRoutine (addCurve, DurationType.Add));
    }

    void OnComboReset () {
        if (animationTask != null) animationTask.Stop (); // stop if running
        
        animationTask = new Task (AnimationRoutine (resetCurve, DurationType.Cooldown));
    }

    IEnumerator AnimationRoutine (AnimationCurve curve, DurationType durationType) {
        var startTime = Time.time;
        float t = 1f;
        while (t > 0) {
            if (t != 1) yield return null; // no yield before we start animating
            transform.localScale = scaleStart * curve.Evaluate (t);
            float duration = GetDuration (durationType);
            t = 1 - Mathf.InverseLerp (startTime, startTime + duration, Time.time);
            if (t == 1) yield return null; // we have to yield here to prevent infinite loops when t = 1 (because Time.time never progresses because the while loop is running infinitely)
        }

        transform.localScale = scaleStart * 0;
    }

    float GetDuration (DurationType durationType) { // this lets us update the duration if it changes in the playerAttack script for some reason
        float returnDuration = 99;
        switch (durationType) {
            case DurationType.Add:
                returnDuration = addDuration;
                break;
            case DurationType.Cooldown:
                returnDuration = playerAttack.lastCooldownDuration;
                break;
            default:
                returnDuration = 99;
                break;
        }

        return Mathf.Max (0.0001f, returnDuration); // no negatives or zero
    }
}
