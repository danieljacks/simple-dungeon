﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDensityByScale : MonoBehaviour {

	void Start () {
		var pSystem = GetComponent<ParticleSystem> ();
		var emission = pSystem.emission;
		emission.rateOverDistanceMultiplier = 1 / transform.lossyScale.x;
	}
}
