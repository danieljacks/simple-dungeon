﻿/*
--------------------------------------------------------------
---------------------TECHNICAL SYSTEMS------------------------
--------------------------------------------------------------

- Make the particle for parrying not a child of the parry attack object. Would require a bit of code editing

- Make player attacks based on an attack value in the player script, rather than a hardcoded value in the slash attack.

- Make some sort of swappable weapon system for player attacks (or any attacks really), maybe having a non-monobehaviour with a reference to a prefab for the attack to basically give some attack multipliers, or color settings etc...

- Make the death system somehow different so it can figure out which death component has to be last (i.e., the one which actually destroys the object)

- Make the Die () function in any death component call a private DieActions() function, then destroy the dead object.
	This should allow multiple death components to work together, by just calling any single one of them

- Make a minimap with minimap fog of war

- Rewrite the attack system (for player at least) so that input is separate from attacks. Input has 4 'slots',
	then attacks, dashes etc implement the ISlot interface, which has an activate function (which works like an update function).
	Each attack has 2 forms: skilled (2 controls) and unskilled (one control). So for melee, one handed is stabbing only (no parry), while two handed allows parrying
	for bows, one handed (I think this is the same thing as the previous todo item)

(DONE) - Rewrite SlasherAttack component to use coroutines rather than update function with Time.time checks. This should make it cleaner and easier to work with


--------------------------------------------------------------
---------------------FEATURE ADJUSTMENTS----------------------
--------------------------------------------------------------

- Make each enemy spawn type a weight to affect its spawn frequency (like my cellular automata project)

- Make the difficulty progression system between regions more consistent

- Make routes not leading to the portal have better loot

- Maybe update the traveling salesman passage generator to purposefully build in dead ends with extra loot

- Make the passage generator less messy (i.e. less unnecessary passageways (although maybe the unecessary ones give flavour...))

- Make fixed bounding boxes for attacks (i.e. not based on the actual movement of the particle, but on a bounding box.) Then make it so that if the player is
	in the bounding box, (i.e. possible to be hit) and facing enemy (say, 180 degrees, with range), then it should DEFINITELY be possible
	to parry. This should mitigate any parrying frustration....

- Add animation for healthbar increase or deacrease

- Make big slimes send out big slime trails with more slime particles per distance. Make slimes scale their size properly with slime size so we can make massive slimes by just changing the slime size

(?) - Make focus 'chaining', i.e. if a nearby enemy sees the player, all within a radius of the first enemy will auto focus on the player too. This makes it harder to peel off one enemy at a time

(?) - Make the random smooth amount for each perlin region use a formula of some sort so most regions have very low smoothing, and very few regions have closer to the max smoothing

(?) - Implement using the next vertex rather than the ventre point to find the perp and radial vectors in the WGenRoom script, like the passage gen script already does. this should reduce distortion when using multiple octaves of noise perturbing

(PASS) - Make MouseFocus use the average position of touch input to determine an aim position. It then aims at anything close to that position, or just aims at the actual position (empty space) if there is nothing there

(PASS) - Create room generator prefab variants with different room settings

(LOW PRIORITY) - Determine whether anchorRadius in FollowMovement component is something positive. Should it be reworked to feel good, (using fixed distance calculations in the circle to feel naturally moving back towards anchor), or just scrapped?

(LOW PRIORITY) - Look into having a list of attacks generated while dashing, so that if an attack is avoided with the dashing invincibility,
	the player cannot be hit by that same attack right when exiting the dash. This may be necessary if thats happening alot.
	Currently the invincibility just last for 30% of the dash duration extra.

(DONE) - See if this works: make it so that parry is checked when the enemy actually attacks, not during the warning signal.
	Basically this means that the player cant interrupt the enemy warning, they have to hold down whichever direction the
	enemy will attack in, thus forcing the player to pay attention to the parry signals, rather than just slashing back
	and forth mindlessly like they can do now

(DONE) - Try making the parry signals use global left and right, rather than local. This may improve readability of the signals,
			Since they will be more consistent for the player

(DONE) - Make the swarmer attack width smaller

(DONE) - Make it so that swiping on player attack works by how long the player holds down on any button (after a certain amount of time, it is not a swipe), rather than when the player last pressed the other direction. This allows the player to not touch anything, and then right away swipe onto
	the correct parry pos without first touching the oppsosite side. Make the cooldown lower gradually, rather than being reset right away, so the player cant just spam on-off-on-off some single direction. Maybe make it connected to the attack cooldown?
	something like, if the attack is not on cooldown, any sort of tap is a parry, otherwise if it is on cooldown (i.e. player is attacking or some such), player must have swiped from opposite side

(DONE) - Currently can just spam left right left right fast to counter any possible attack. Maybe make the attack animation longer,
	and add a short cooldown which is present even when correctly alternating hands.
	Update: implemented. Now you can just hold down the button where you know it will attack next... Maybe make it a swipe, i.e. you have to
	start the warning period on the wrong side, then swipe over to the correct side before the timers up to sucessfully parry. And you still wont attack during parry period as it is now

--------------------------------------------------------------
---------------------REFACTOR AND OPTIMIZE--------------------
--------------------------------------------------------------

- Rewrite parrying because it is a mess of hacky fixes right now. Decide on a new method of communication between
	PlayerAttack and SlasherAttack.

- Make the MouseFocus component use a collider object for efficiency

- Optimize SlasherMovement script to not use so many Distance calls

- Refactor PlayerAttack to use more coroutines, rather than Time.time comparisons

- Serious optimizing for the perlin gen system. Currently messy and uses multiple GridManager.SetCell calls. Should cache anything it can, and do all modifications in the grid before applying them. Also, WGenPassage shouldnt directly mosify the grid, but should return a set of points, or maybe modify an array. Maybe make it not a MonoBehaviour?

(DONE) - Rewrite grid system and room generator's interaction with grid system so its less laggy and doesnt randomly make multiple copies of walls


--------------------------------------------------------------
------------------------GAMEPLAY AND UI-----------------------
--------------------------------------------------------------

- Show some playtesters the demo and get feedback

- Make the slashing ui arrows have a circular highlight which is the full size of the button when the user starts pressing it, 
	but then gets gradually smaller to indicate when the press is still considered a 'swipe' for parrying purposes. Extra feedback for players.

(DONE) - Make a playable demo

--------------------------------------------------------------
----------------------RESEARCH AND IDEAS----------------------
--------------------------------------------------------------

- Look into implementing a stunlock like in Dead Cells

- Make spinning attack (just spin the player or enemy somehow, even make them spin themself off a wall? looks great)

IDEA!!!!
- Make parrying like dance dance revolution, where the parry arrows float towards the player, and the player has to swipe to the correct arrow at the correct moment to parry. This basically creates a stream of arrows... 
	the system is quite similar to what is implemented now, except the swipe has to be right at the end and the arrows move...

- Some sort of color or aesthetic generation. We already have regions, with associated difficulty levels. Maybe
	make the color a gradient between two or three colors, with the gradient progress based on difficulty.
	What about decor? different themed rooms? different wall/floor textures? glowing crystal things?
	Maybe make a decor generator which separately generates these stuff after the enemy gen.

- Maybe make difficulty based on room size, or just connect the rooms up from smallest to largest (if thats
	even possible). Because currently if the latter rooms are small, they have tons of enemies, whereas the
	initial rooms might be massive, but with no one there.

- Make distance records that give alot of money

- Frenzy power (either from store or random powerup, or spell etc...) Makes time slow down whenever there is an
	opportunity to parry. Need a TimeManager class for this. then speeds back up when the player cant attack

- Alternative control styles: 
	simply take out parry and only focus on dodge (like wizard of legend)
	use a timed single press block (like in dead cells)
	have the indicators move towards player and have them parry in sequence to deal with large groups (do we even want the parry blade to be able to deal with large groups?)

- Make simple parry system. Two directions for slashing: left to right and right to left. slashing in the 
	opposite direction as the enemy will parry, effectively negating both attacks? or doing extra damage and
	negating the enemy's attack? or maybe it does damage equal to the difference in the player's attack vs the enemy's?
	(so parrying is stronger with a high damage weapon, but easier with a high speed weapon. But having high speed
	weapon also means regular hits can be got in. Maybe parrying with alot of damage does ALOT of bonus damage,
	or stuns the enemy for a short while...)
	- The idea I like best: parry by swinging in the opposite direction as the enemy. high damage compared to enemy
		stuns enemy and does damage. (is the converse also true? i.e. can player be stunned?). weapons with higher
		attack have lower attack speeds.
	- How does dodging come into play? (i.e. a weapon with low damage and high speed is not suitable for parrying.
		So how would such a player dodge?)

- Have a Player attack controller, which basically handles input, figures out whats considered a swipe, and handles cooldowns.
	has unity events to tell any other interested component. Events might include a swipe to direction event, with an
	argument saying whether the swipe was from a previous direction or from nothing, an event to indicate a successful
	parry, an event for up arrow, and an event for down arrow. So maybe something like:
	an event for each arrow, which has an eventArg which says whether the press was a swipe, or a hold down, and where the last direction was.
	A parry blade might use that 

- Maybe bows work like this: player holds down an arrow key to make aiming reticule smaller, which increases accuracy,
	then they swipe to the other direction to fire

- Look into making it possible to hold down only one direction, while still slashing in both directions, and using the
	directional system only for parrying (would then be really important to indicate to the player when what they did is
	considered a swipe). This might make it more comfortable for the player to control. Get playtester feedback on whether
	the swipe control system is annoying. Also, this system might be absolutely necessary if there is ever a rapid attack
	weapon (cant have the player swiping 5 times a second)

- Think about having a leveling system where the player can go back to the surface at any time through a portal at the
	beginning of a level, so they have to choose how far they go. But if they die, they lose all their coins (or maybe
	a percentage of it). (a bit like the mine in harvest moon) I feel like this lets players choose their level of difficulty (since they can go to levels
	that they are actually unequipped to deal with, or stay at the easier levels) and lets them take risks. Maybe
	there is an upgrade which allows them to keep some percentage of coins on death, like an expedition or something
	which goes out to find them. Or even better, the player can go back through the same dungeon and find their corpse
	with a large percentage of their gold. This also means the player can prepare for the dungeon, so they might not die
	like the first time. Maybe rewards get less and less the more the player plays one dungeon, to encourage exploration
	of different dungeons, and ensure that the player doesnt just redo one dungeon where a lot of chests happened to spawn
	over and over.

- Powerup where attack cooldown is lowered to less than the time it takes to actually complete the attack, so the player can have both LR and RL
	attacks going at the same time. Looks really cool and must be fun too

- New controls scheme based on playtester input: Combo system, where different 'elements' (for melee, this might
	be attack, defense, movement, for magic, maybe fire, water, earth, energy, damage, structure, movement. 
	Ranged might have ?? a grid system, where the player has to manually aim through combos ?? maybe just the
	same as melee...) Player can press (or maybe swipe also) on the different elements, which build up in a combo
	box (like magicka). Player can then press clear to clear combo, or attack to activate combo. But the attack
	button can be held down, to keep using the same combo. This means simple combos can be automatically repeated
	by just holding down the button.

- Magic: as said above, will use a combo system. Mana (or maybe it should be called focus or memory) should 
	work something like this: every spell which exists in the gameworld occupies a certain number of mana 
	points. Spells do have cooldowns, but they are very short. Spells do include agents, so if the player 
	has a high mana level, they can have multiple agents active at once. They can also just fire multiple 
	fireballs in sequence. Of course, some spells (e.g. agents or walls) benifit from very high 
	levels of mana.

--------------------------------------------------------------
----------------------DESIGN CHOICES--------------------------
--------------------------------------------------------------
- Parry system: player touches left or right arrows to attack towards the left or right. Enemies can also attack left or right, and will indicate
	which direction they will attack for a bit before the attack. The player is considered to have swiped onto a direction if they were recently
	pressing a different direction, or no direction, and then start pressing the direction they are swiping onto. If the player swipes onto the 
	indicated direction while an enemy is signaling its attack, and is still holding down that direction when the enemy attacks, 
	the player parrys the attack.

- Weapons: 

	- have the parry blade be a dueling weapon, so it is very strong one on one, but not so much against groups. 
	- Then have a shield, which blocks in bursts and damages anyone who hits it. 	This is best for groups, since they all attack in unison. Also, this makes it useful to engage large groups all at once, since you can block everyone anyway. 
	- Then have a bow, which is ranged, and works well against ranged enemies like slimes, where you dont really want to get close. The shield and bow would be more traditional weapons though, and each weapon should have ways to deal with the other enemy types, so bows should technically be able to defeat one on one, but it would be harder. Maybe duelist enemies do more damage (so one hit from a dueler is the same damage as one hit from each member of a group)
	- Then have magic, which is powerful and ranged, but also gives enemies powerups while damaging them, for instance
	fire magic does damage, but makes enemies run fast and have larger sight radius and damage, meaning the player 
	needs distance or traps to stop enemies. Ice magic might freeze enemies, but make them immune to damage, giving
	the player time to set up traps, but being innefective in doing damage
	- Traps: these have a very high payoff (i.e. instant death, complete immobilization) but have to be layed in advance,
	while not taking damage. Maybe starting to lay a trap alerts all enemies in an area an makes them run faster 
	to the player, and the laying of the trap can be interrupted, putting it on a very long cooldown. Also, the player
	has to make the enemies walk into it. Maybe enemies avoid traps unless they are about to attack, or player has to
	use other invisible pushing traps or magic to use them


- Make dashing a charge system, so player can use multiple dashes in a row to quickly close distance, but cant use unlimited dashes to just always avoid damage

- Make a bow attack to use in a new Down Arrow slot, and make a grapple, which is basically a long range dash, but can only be used near walls

 */