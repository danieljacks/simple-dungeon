﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CatchCo;

public class Testing : MonoBehaviour {
	public Vector2 textureSize = new Vector2 (1, 1);
	public Vector2Int texturePixels = new Vector2Int (200, 200);
	public int loopX = 0;
	public int loopY = 0;
	public float period = 1.0f;
	public int noiseSeed = 1337;

	private Texture2D noiseTex;


	[ExposeMethodInEditor]
	void Regenerate () {
		noiseTex = new Texture2D (texturePixels.x * 3, texturePixels.y * 3);

		FastNoise noise = new FastNoise ();
		noise.SetFrequency (1 / period);
		noise.loopX = loopX;
		noise.loopY = loopY;
		noise.SetSeed (noiseSeed);
		noise.SetNoiseType (FastNoise.NoiseType.Perlin);

		for (int y = 0; y < noiseTex.height; y++) {
			for (int x = 0; x < noiseTex.width; x++) {
				float noiseValue = SimplexNoise.SeamlessNoise ((float)x / (float)noiseTex.width, (float)y / (float)noiseTex.height, period, period, noiseSeed);
				noiseValue = (noiseValue + 1) * 0.5f;
				noiseTex.SetPixel (x, y, new Color (noiseValue, noiseValue, noiseValue));
			}	
		}

		noiseTex.Apply ();

		var renderer = GetComponent<SpriteRenderer> ();
		renderer.sprite = Sprite.Create (noiseTex, new Rect (0, 0, noiseTex.width, noiseTex.height), new Vector2 (0.5f, 0.5f));
	}
}
