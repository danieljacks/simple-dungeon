﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Agent {

	// TODO: clean up code, fix lag. Make trimming work

	public enum AgentState {
		Moving,
		Trimming,
		Filling,
		Idle
	}

	public float roomSize = 3.0f;
	public float speed = 3.0f;
	public float stepSize = 0.005f;
	public float startAngle = 0;
	public float edges = 4;
	public float roughness = 0.5f;
	public float jaggedness = 0.5f;
	public float irregularity = 0.0f;
	public int trimSpeed = 3;
	public int maxStalactiteSize = 1;
	[HideInInspector]
	public AgentState currentState = AgentState.Moving;

	private float currentAngle;
	private float idealAngle;
	public Vector2 idealPosition;
	// multiplier to keep up with the ideal position
	public float idealSpeedMultiplier;
	private float circleAngle;
	private float nextChangeAngle;
	private float nextChangeDirection;
	// position works in cells, i.e. one x is one grid cell, regardless of worldspace size
	public Vector2 currentPosition;
	private Vector2 prevPos;
	private int currentTick;
	private int trimIndex;
	private List<Vector2Int> changedCells; // TODO: implement cecking for done condition that it is not bumping into the last few cells (might this cause infinite loops when the entire thing is only a few cells big? maybe have some sort of timeout or minimum room size...)
	private float adjustedSpeed;

	
	public Agent () {
		ResetVariables ();
	}

	private void ResetVariables () {
		// this is a separate method, because some methods (e.g. singleton) cannot be accessed at certain constructor calls (e.g. serialization)
		currentAngle = idealAngle = circleAngle = startAngle;
		nextChangeDirection = currentTick = 0;
		nextChangeAngle = 0;
		currentPosition = prevPos = idealPosition = Vector2.zero;
		trimIndex = 0;
		changedCells = new List<Vector2Int>();
		currentState = AgentState.Moving;
		idealSpeedMultiplier = 0;
		adjustedSpeed = speed;
	}

	public void Restart () {
		ResetVariables ();

		GridManager.Instance.FillAll (CellType.Wall);
	}
	

	public void Tick () {
		switch (currentState) {
			case AgentState.Moving:
				MovingTick ();
				break;
			case AgentState.Trimming:
				TrimmingTick ();
				break;
			case AgentState.Filling:
				FillingTick ();
				break;
			default:
				break;
		}

		Debug.Log (adjustedSpeed);
	}

	void MovingTick () {

		if (currentTick >= nextChangeDirection) {
			nextChangeDirection = currentTick +  1 / (roughness * 0.1f);
			ChangeDirection ();
		}

		//if (circleAngle < 360) {
			if (circleAngle >= nextChangeAngle) {
				nextChangeAngle += 360 / edges;
				ChangeIdealAngle ();
			}


			for (int i = 0; i < adjustedSpeed ; i++) {
				circleAngle += 1 / (roomSize * GridManager.Instance.cellSize);

				// move ideal agent
				Vector2 idealTranslate = new Vector2 (Mathf.Cos(idealAngle * Mathf.Deg2Rad), Mathf.Sin (idealAngle * Mathf.Deg2Rad));
				idealPosition += idealTranslate * stepSize;
			}
		//}

		AdjustSpeed ();

		prevPos = currentPosition;
		for (int i = 0; i < speed; i++) {
			Move ();
			currentTick += 1;
		}
		
	}

	void TrimmingTick () {
		for (int i = 0; i < trimSpeed; i++) {
			trimIndex += 1;
			if (trimIndex >= changedCells.Count) {
				currentState = AgentState.Filling;
				return;
			}

			List<Vector2Int> directions = new List<Vector2Int> {Vector2Int.up, Vector2Int.down, Vector2Int.right, Vector2Int.left};

			List<Vector2Int> neighbours = new List<Vector2Int> ();
			List<Vector2Int> stalactite = new List<Vector2Int> ();
			
			Vector2Int p = changedCells[trimIndex];
			while (true) {
				neighbours.Clear ();

				for (int d = 0; d < 4; d++) {
					// only counted as a new neighbour if we never looked at that cell before
					if (!stalactite.Contains (p + directions[d]) && GridManager.Instance.GetCell(p + directions[d]) == CellType.Empty) {
						neighbours.Add (p + directions[d]);
					}
				}

				if (neighbours.Count == 0) {
					GridManager.Instance.SetCell (neighbours[0], CellType.Wall);
					break;
				}
				if (neighbours.Count == 1) {
					stalactite.Add (p);
					p = neighbours[0];
				} else {
					if (stalactite.Count > maxStalactiteSize) {
						foreach (Vector2Int position in stalactite) {
							GridManager.Instance.SetCell (position, CellType.Wall);
						}
					}
					break;
				}
			}
		}

	}

	void FillingTick () {

	}

	void Move () {
		// move agent
		Vector2 translate = new Vector2 (Mathf.Cos(currentAngle * Mathf.Deg2Rad), Mathf.Sin (currentAngle * Mathf.Deg2Rad));

		Vector2 circuitTranslate = Vector2.zero;
		float completeCircuitMultiplier;
		if (changedCells.Count > 5) {
			Vector2 initialCellPos = GridManager.Instance.GridToWorldPos (changedCells[0]);
			completeCircuitMultiplier = Vector2.Dot ((currentPosition - prevPos).normalized, (initialCellPos - currentPosition).normalized);
			completeCircuitMultiplier = Mathf.Sign(completeCircuitMultiplier) * Mathf.Pow (completeCircuitMultiplier, 4); // means only values close to one have any impact. Sign is there because x^4 is always positive. We want to preserve the sign
			completeCircuitMultiplier = 0.5f + 0.5f * completeCircuitMultiplier;
			circuitTranslate = (initialCellPos - currentPosition).normalized * 5f * completeCircuitMultiplier * Mathf.Min((1 / Vector2.SqrMagnitude((initialCellPos - currentPosition) / GridManager.Instance.cellSize)), 1);
		} 

		Vector2 newPos = currentPosition + (translate + circuitTranslate) * stepSize; // 0.25, or 1 / 4 of a cell, so that cells usually wont be skipped (still can be, but we can check for that later)

		if (CheckDone(currentPosition, newPos)) currentState = AgentState.Filling; // dont move if we are done
		currentPosition = newPos;


		if (GridManager.Instance.CheckInGridBounds (currentPosition)) {
			GridManager.Instance.SetCell (currentPosition, CellType.Empty);

			Vector2Int gridPos = GridManager.Instance.WorldToGridPos (currentPosition);
			if (!changedCells.Contains (gridPos)) {
				changedCells.Add (gridPos);
			}
		}
	}

	bool CheckDone (Vector2 oldPos, Vector2 newPos) {
		Vector2Int oldGridPos = GridManager.Instance.WorldToGridPos (oldPos);
		Vector2Int newGridPos = GridManager.Instance.WorldToGridPos (newPos);

		if (oldGridPos != newGridPos && GridManager.Instance.GetCell(newGridPos) == CellType.Empty 
		&& changedCells.Contains (newGridPos) && changedCells.IndexOf (newGridPos) < changedCells.Count - 4) {
			return true;
		} else {
			return false;
		}
	}

	void ChangeDirection () {
		float rnd = Random.value;

		float offset = Mathf.Lerp (-jaggedness, jaggedness, rnd);
		float correctionPosOffset = Vector2.SignedAngle (idealPosition, currentPosition) * Mathf.InverseLerp(0, roomSize * GridManager.Instance.cellSize / 4, Vector2.Distance(currentPosition, idealPosition));
		correctionPosOffset *= 0.4f;
		float correctionAngleOffset = idealAngle - currentAngle; // ensures we are never too far off idealAngle
		correctionAngleOffset *= 0.6f; // correction strength, 0..1 range
		if (changedCells.Count > 0) {
			//float completeCircuitMultiplier = 0.5f + 0.5f * Vector2.Dot (currentPosition - prevPos, currentPosition - changedCells[0]); // only activates if it is facing more or less the correct general direction
			//float circuitDistanceMultiplier = ( 1 /Mathf.Lerp (0.01f, 1, Mathf.InverseLerp(0, roomSize * GridManager.Instance.cellSize / 4, Vector2.Distance(currentPosition, idealPosition))));
			//float completeCircuitOffset = completeCircuitMultiplier * Vector2.SignedAngle (changedCells[0], currentPosition) * circuitDistanceMultiplier;
			//completeCircuitOffset *= 0.5f;

			//currentAngle += completeCircuitOffset;
		}

		currentAngle += offset + correctionPosOffset + correctionAngleOffset;
	}

	void AdjustSpeed () {
		idealSpeedMultiplier = Vector2.Dot ((currentPosition - prevPos).normalized, (idealPosition - currentPosition).normalized);
		adjustedSpeed = speed - idealSpeedMultiplier * speed * 0.6f;
	}

	void ChangeIdealAngle () {
		float rnd = Random.value;

		float irregularityOffset = Mathf.Lerp (-irregularity, irregularity, rnd);
	
		idealAngle = circleAngle + irregularityOffset;
		currentAngle = Mathf.LerpAngle (currentAngle, circleAngle, 1f);
		ChangeDirection ();
	}
}
