﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Region {
    public HashSet<Vector2Int> _cells = new HashSet<Vector2Int> ();
    public HashSet<Vector2Int> cells {
        get {
            return _cells;
        }
        set {
            _cells = value;
        }
    }
    public int area {
        get {
            return cells.Count;
        }
    }

    private List<Vector2Int> directions = new List<Vector2Int> () {Vector2Int.right, Vector2Int.up, Vector2Int.left, Vector2Int.down};

    public RectInt GetBounds () {
        if (cells.Count == 0) return new RectInt (0, 0, 0, 0);

        float minX = Mathf.Infinity;
        float maxX = Mathf.NegativeInfinity;
        float minY = Mathf.Infinity;
        float maxY = Mathf.NegativeInfinity;

        foreach (var pos in cells) {
            if (pos.x < minX) minX = pos.x;
            if (pos.x > maxX) maxX = pos.x;
            if (pos.y < minY) minY = pos.y;
            if (pos.y > maxY) maxY = pos.y;
        }

        var rectWidth = maxX - minX + 1; // I believe the +1 is necessary, because if the maxX = minX (i.e. only one block thick) the area width should be 1 not 0
        var rectHeight = maxY - minY + 1;

        return new RectInt (Mathf.RoundToInt(minX), Mathf.RoundToInt(minY), Mathf.RoundToInt(rectWidth), Mathf.RoundToInt(rectHeight));
    }

    public Vector2Int GetCenterPos () {
        int sumX = 0;
        int sumY = 0;

        foreach (var wall in cells) {
            sumX += wall.x;
            sumY += wall.y;
        }

        var centerX = Mathf.RoundToInt ((float) sumX / (float) cells.Count);
        var centerY = Mathf.RoundToInt ((float) sumY / (float) cells.Count);
        var center = new Vector2Int (centerX, centerY);

        if (!cells.Contains(center)) { // for instance, a mound shaped region (/-\) may have the center of mass in the middle section, but actually outisde the region. The center should always be an actual cell and not outside the region
            Vector2Int closestPos = center;
            float closestDistance = Mathf.Infinity;
            foreach (var cell in cells) {
                var distance = Vector2.Distance (center, cell);
                if (distance < closestDistance) {
                    closestDistance = distance;
                    closestPos = cell;
                }
            }

            center = closestPos;
        }

        return center;
    }

    public bool IsEdgeRegion () {
        foreach (var pos in cells) {
            foreach (var direction in directions) {
                if (!GridManager.Instance.CheckInGridBounds(pos + direction)) {
                    return true;
                }
            }
        }

        return false;
    }

    public void RecalculatePositions () {
        float minX = Mathf.Infinity;
        float maxX = Mathf.NegativeInfinity;
        float minY = Mathf.Infinity;
        float maxY = Mathf.NegativeInfinity;

        var leftWalls = new List<Vector2Int> ();
        var rightWalls = new List<Vector2Int> ();
        var topWalls = new List<Vector2Int> ();
        var bottomWalls = new List<Vector2Int> ();

        foreach (var wall in cells) {
            if (wall.x < minX) {
                minX = wall.x;
            }
            if (wall.x > maxX) {
                maxX = wall.x;
            } 
            if (wall.y < minY) {
                minY = wall.y;
            } 
            if (wall.y > maxY) {
                maxY = wall.y;
            } 
        }
    }
}
