﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OLD_Agent : MonoBehaviour {

	// how big is the room which the agent carves out
	public float roomSize = 10.0f;
	// 1 = perfectly smooth, 0 = very jagged
	public float roomSmoothness = 0.5f;
	// how much the room deviates from a 'perfect' shape, i.e. a hexagon or circle. higher values result in more irregular walls. very high values may have extreme results
	public float roomIrregularity = 0.5f;
	public bool clockwise = false;

	public int speed = 1;


	private float roomCurve;
	// the furthest angle in the chosen direction for this agent, the agent generally wants to go higher than this value. The value increases as the agent completes the room
	private float furthestAngle;
	private float idealAngle;
	private float startAngle = 0;
	private float lastDirectionChange = 0;
	public float currentAngle;
	private float nextChangeAngle;
	private float numberOfEdges;


	void Start () {
		roomCurve = 1 / roomSize;
		currentAngle = startAngle;
		idealAngle = startAngle;
		nextChangeAngle = startAngle;
		numberOfEdges = GetNumberOfEdges (roomSmoothness);
	}

	void Update () {

		//float timeToComplete = 2 * Mathf.PI * (roomCurve/360) / speed;
		//float timeBetweenDirectionChange = timeToComplete * roomSmoothness / 4;


		if (idealAngle >= nextChangeAngle) {
			
			nextChangeAngle += 360 / numberOfEdges;
			ChangeDirection ();
		}

		for (int i = 0; i < speed; i++) {
			Move ();
		}
	}

	float GetNumberOfEdges (float smoothness) {
		// if modifying the magic numbers, careful that it never goes below 3 edges
		float skewedNumberOfEdges = (179 * 360 * (smoothness - 0.6f) ) / (1 + Mathf.Abs (360 * (smoothness - 0.6f))) + 181.5f;// makes it less likely to get random intermittent numbers, like 19 or 132, and more likely to get very small (3,4) or very large (360) ones
		float intEdges = Mathf.RoundToInt (skewedNumberOfEdges);

		float diff = Mathf.Abs (skewedNumberOfEdges - intEdges);
		float partialEdges = Mathf.Lerp (-diff, diff, Random.value) * roomIrregularity;
		
		return intEdges + partialEdges;
	}

	public void ChangeDirection () {
		float rnd = Random.value;

		float irregularityOffset = Mathf.Lerp (-roomIrregularity, roomIrregularity, rnd); // random change from ideal angle means irregular rooms
	
		currentAngle = idealAngle + irregularityOffset; //+ wrongDirectionOffset; // now change angent's direction
	}

	public void OLD_ChangeDirection () {
		//GridManager.Instance.SetCell (GridManager.Instance.WorldToGridPos (transform.position), CellType.Wall);

		// if smoothness is 1, the angle should increase with curveStrength. otherwise, should be some sort of dropoff function, where the variance is dictated by how large the roughness value is
		// maybe:

		float rnd = Random.value;

		// we use a point-slope graph with the gradient based on an angle for a linear probability curve, which includes both positive and negative angle offset values
		float curveSkew = 15f; // 1 is a linear curve, smaller values mean there is a larger probability for smaller offsets as opposed to larger ones.
		float gradient = curveSkew * Mathf.Tan (roomIrregularity * Mathf.PI / 2);
		float angleOffset = gradient * (rnd - 0.5f); // linear probability curve

		float wrongDirectionOffset = Mathf.Clamp (idealAngle - currentAngle, -45, 45) * 0.2f; // this is positive when the agent is going in the opposite direction to its rotation. Keeps it on track

		currentAngle = idealAngle + angleOffset; //+ wrongDirectionOffset; // now change angent's direction
		// TODO: make this so that you can make perfect squares... I think it doesnt work now because the timings do not match up mathematically
	}

	public void Move () {
		idealAngle += roomCurve;

		// move agent
		Vector2 translate = new Vector2 (Mathf.Cos(currentAngle * Mathf.Deg2Rad), Mathf.Sin (currentAngle * Mathf.Deg2Rad));
		transform.Translate (translate * Time.deltaTime); //remove the time.delta time for it to go faster, then you can set speed to make sure no cells are skipped
	}
}
