﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CatchCo;

public class WGenPassage : MonoBehaviour {
    [System.Serializable]
	public class OctaveData {
		public int subdivisions = 1;
		public float noiseFrequency = 1.0f;
		public float radialNoiseStrength = 1.0f;
		public float perpendicularNoiseStrength = 1.0f;
	}

    public Vector2 startPoint = Vector2.zero;
    public Vector2 endPoint = Vector2.right * 10;
    public float passageWidth = 2.0f;

	[HideInInspector]
	public List<Vector2> shapeVertices = new List<Vector2> ();
	[HideInInspector]
	public List<Vector2> vertices = new List<Vector2> ();
	public Vector2 centre = Vector2.zero;
	public float roomSize = 3.0f;
	public List<OctaveData> octaves = new List<OctaveData> ();
	public int noiseSeed = 1337;

	public GameObject vertexMarkerPrefab;
	public GameObject shapeVertexMarkerPrefab;

	private List<GameObject> markers = new List<GameObject> ();
	public float noiseOffset = 0f;

	void Start () {
		//Regenerate ();
	}

    [ExposeMethodInEditor]
    void RandomOffset () {
        noiseOffset = Random.Range (0f, 9999f);
    }

	[ExposeMethodInEditor]
	public void Regenerate () {

		//DeleteMarkers ();
		shapeVertices.Clear ();
		vertices.Clear ();

		//GridManager.Instance.FillAll (CellType.Wall);

		GenerateShapeVertices ();

		for (int i = 0; i < octaves.Count; i++) {
			if (octaves.Count == 0) break;
			SubdivideVertices (i);
		}

		//GenerateMarkers ();

		CreateRoomInGrid ();
	}

	void DeleteMarkers () {
		foreach (Transform marker in transform) {
			if (Application.isPlaying) {
				Destroy (marker.gameObject);
			} else {
				DestroyImmediate (marker.gameObject);
			}
		}
		markers.Clear ();
	}

	private void CreateRoomInGrid () {
		var points = new List<Vector2Int> ();
		for (int y = 0; y < GridManager.Instance.gridHeight; y++) {
			for (int x = 0; x < GridManager.Instance.gridWidth; x++) {
				var worldPos = GridManager.Instance.GridToWorldPos (new Vector2Int (x, y));
				if (IsInsideRoom (worldPos)) {
					GridManager.Instance.SetCell (worldPos, CellType.Empty);
					points.Add (new Vector2Int (x, y));
				}
			}
		}
		var hi = "test";
	}

	private void GenerateMarkers () {
		foreach (var shapeVertex in shapeVertices) {
			var GO = Instantiate (shapeVertexMarkerPrefab, shapeVertex, Quaternion.identity, transform) as GameObject;
			markers.Add (GO);
		}

		for (int i = 0; i < vertices.Count; i++) {
			var GO = Instantiate (vertexMarkerPrefab, vertices[i], Quaternion.identity, transform) as GameObject;
			GO.GetComponent<lineMarker>().nextPos = vertices[i + 1 >= vertices.Count ? 0 : i + 1];
			markers.Add (GO);
		}

		var v3Array = new List<Vector3>();
		foreach (var v in vertices) {
			v3Array.Add(new Vector3 (v.x, v.y, 0));
		}

		var lineRenderer = GetComponentInChildren<LineRenderer>();
		lineRenderer.transform.position = transform.position;
		lineRenderer.positionCount = v3Array.Count;
		lineRenderer.SetPositions(v3Array.ToArray());
	}

	float GetNoiseRadial (float t, int octave) {
		float frequency = octaves[octave].noiseFrequency;
		float strength = octaves[octave].radialNoiseStrength;

		float noiseValue = SimplexNoise.SeamlessNoise (t, 0, frequency, frequency, noiseOffset + frequency * 13);
		noiseValue = noiseValue * strength * roomSize * 0.1f;
		return noiseValue;
	}

	float GetNoisePerp (float t, int octave) {
		float frequency = octaves[octave].noiseFrequency;
		float strength = octaves[octave].perpendicularNoiseStrength;

		float noiseValue = SimplexNoise.SeamlessNoise (0, t, frequency, frequency, noiseOffset + frequency * 13 + 1367);
		noiseValue = noiseValue * strength * roomSize * 0.1f;
		return noiseValue;
	}

	public void SubdivideVertices (int octave) {
		List<Vector2> oldVertices = new List<Vector2> (vertices.Count != 0 ? vertices : shapeVertices); // shallow copy

		int subdivisions = octaves[octave].subdivisions;
		int verticesAdded = 0;

		for (int i = 0; i < oldVertices.Count; i++) {
			var v0 = oldVertices[i];
			int i1 = i + 1 < oldVertices.Count ? i + 1 : 0;
			var v1 = oldVertices[i1];

			float subLength = Vector2.Distance (v0, v1) / (float) (subdivisions + 1);
			Vector2 dir = (v1 - v0).normalized;

			for (int s = 0; s < subdivisions; s++) {
				Vector2 delta = dir * (subLength + s * subLength);
				Vector2 vertexToAdd = PerturbVertex (v0 + delta, verticesAdded, i * subdivisions, octave);
				vertices.Insert (1 + i + verticesAdded, vertexToAdd); // keep old vertices order
				verticesAdded++;
			}
		}
	}

	private void GenerateShapeVertices () {
		shapeVertices.Clear ();

        Vector2 v0, v1, v2, v3;
        Vector2 forward = (endPoint - startPoint).normalized;
        Vector2 perp = Vector2.Perpendicular (forward);

        v0 = startPoint - forward * passageWidth - perp * passageWidth;
        v1 = endPoint + forward * passageWidth - perp * passageWidth;
        v2 = endPoint + forward * passageWidth + perp * passageWidth;
        v3 = startPoint - forward * passageWidth + perp * passageWidth;

        shapeVertices.AddRange (new Vector2[]{v0, v1, v2, v3});

		vertices = new List<Vector2> (shapeVertices);
	}

	private Vector2 PerturbVertex (Vector2 vertex, int vertexNumber, int totalVertices, int octave) {
		var returnVertex = vertex;

		if (totalVertices == 1) return vertex; // would give us a NaN in a few lines
		Vector2 perp;
		if (vertexNumber + 1 < vertices.Count) { // you can convert this to use the next and previous shapeVertex to get the perp and radial vectors if using the next (possibly already perturbed) vertex doesnt work
			perp = (vertices[vertexNumber + 1] - vertex).normalized;
		} else if (vertexNumber - 1 >= 0) {
			perp = (vertex - vertices[vertexNumber - 1]).normalized;
		} else {
			perp = Vector2.Perpendicular ((vertex - centre).normalized);
		}

		Vector2 radial = Vector2.Perpendicular (perp);
		float t = (float) vertexNumber / (float) (totalVertices - 1);
		returnVertex += radial * GetNoiseRadial(t, octave) + perp * GetNoisePerp (t, octave);

		for (int i = 0; i < 4; i++) { // make sure we dont have an intersection
			if (IsInsideRoom (returnVertex)) {
				returnVertex = Vector2.Lerp (vertex, returnVertex, 0.5f);
			} else {
				continue;
			}
		}

		if (IsInsideRoom (returnVertex)) {
			returnVertex = vertex;
		}
		if (IsInsideRoom (returnVertex)) {
			//print ("a vertex is still intersecting");
		}

		return returnVertex;
	}

	bool IsInsideRoom (Vector2 point) { 
		var j = vertices.Count - 1;
		var inside = false;
		for (int i = 0; i < vertices.Count; j = i++) { 
			if ( ((vertices[i].y <= point.y && point.y < vertices[j].y) || (vertices[j].y <= point.y && point.y < vertices[i].y)) && 
				(point.x < (vertices[j].x - vertices[i].x) * (point.y - vertices[i].y) / (vertices[j].y - vertices[i].y) + vertices[i].x)) 
				inside = !inside; 
		} 
		return inside; 
	}
}
