﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CellM : MonoBehaviour {

	public abstract CellType CellType {get;}
	[HideInInspector]
	public int id;
	public Vector2Int gridPos;


	private static int idCounter = 0;
	public static int GetNextID () {
		return ++idCounter;
	}

	private bool hasGridPos = false;

	public virtual void Awake () {
		id = GetNextID ();
	}
}
