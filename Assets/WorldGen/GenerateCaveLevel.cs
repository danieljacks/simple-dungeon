﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CatchCo;
using System.Linq;
using UnityStandardAssets._2D;

[RequireComponent(typeof(WGenPerlin))]
public class GenerateCaveLevel : MonoBehaviour
{
    [System.Serializable] public class EnemyType {
        public GameObject prefab;
        public float difficulty = 1.0f; // works kind of like a quota, where a more difficult room will attempt to put harder enemies (and more of them)
        [Tooltip("Use 0 radius for an enemy which takes up no space")] public float size = 1f;
        public bool rotateSprite = true;
        [Tooltip("Higher numbers mean more of this enemy type. Probability of this enemy type is (weightedProbability) / (sum of all weighted probabilities)")] public float weightedProbability = 1;
        [Range(0, 1)][Tooltip("Basically, higher values mean smaller average group size. Recommended 0.2..0.8 range")]public float groupSizeReturnChance = 0.5f; //basically, higher values mean smaller average group size. TODO: make this automatically calculated based on an inputted average group size, then use a formula to get this probability
    
        public int GridRadius {
            get{
                var radius = size / 2f;
                return Mathf.RoundToInt ((radius - 0.1f) / GridManager.Instance.cellSize); // -0.1 is just so 0.5 rounds down...
            }
        }

        public int UseArea {
            get {
                return Mathf.RoundToInt(size * 20); // i.e. bigger than the actual size
            }
        }
    }

    public GameObject playerPrefab;
    public GameObject cameraPrefab;
    public GameObject endPortalPrefab;
    public float minRegionDifficulty;
    public float maxRegionDifficulty;

    public List<EnemyType> enemyTypes = new List<EnemyType> ();
    private HashSet<Vector2Int> enemiesPos = new HashSet<Vector2Int> ();
    

    private WGenPerlin perlinGen;

    void Start () {
        GetComponent<WGenPerlin> ().RandomizeSeed ();
        GenerateLevel ();
    }

    [ExposeMethodInEditor]
    public void GenerateLevel () {
        ResetVariables ();

        perlinGen = GetComponent<WGenPerlin> ();

        var oldState = Random.state;
        Random.InitState (perlinGen.seed);

        perlinGen.GeneratePerlin ();

        if (perlinGen.regions.Count <= 1) {
            Debug.Log ("Not enough regions!");
            return;
        }

        var startRegion = perlinGen.regions[0];
        var endRegion = perlinGen.regions[perlinGen.regions.Count - 1];
        var player = SpawnAtGridPos (playerPrefab, startRegion.GetCenterPos());
        var camera = SpawnAtGridPos (cameraPrefab, startRegion.GetCenterPos());
        camera.GetComponent<Camera2DFollow> ().target = player.transform;
        camera.transform.position = new Vector3 (camera.transform.position.x, camera.transform.position.y, -10f);
        camera.transform.parent = transform;
        SpawnAtGridPos (endPortalPrefab, endRegion.GetCenterPos());
  
        int numberOfRegions = perlinGen.regions.Count - 2; // -2 because start and end regions arent counted
        float difficultyDelta = (maxRegionDifficulty - minRegionDifficulty) / (float) (numberOfRegions);
        float difficulty = difficultyDelta;
        foreach (var region in perlinGen.regions) {
            if (region == startRegion || region == endRegion) continue;

            SpawnEnemies (region, difficulty);

            difficulty += difficultyDelta;
        }

        print ("end difficulty: " + difficulty);

        Random.state = oldState;
    }

    EnemyType RandomEnemyType () {
        if (enemyTypes.Count == 0) return null;

        float probabilitySum = 0;
        foreach (var enemyType in enemyTypes) {
            probabilitySum += enemyType.weightedProbability;
        }

        var rnd = Random.Range (0, probabilitySum);

        float currentNetProbability = 0;
        foreach (var enemyType in enemyTypes) {
            currentNetProbability += enemyType.weightedProbability;
            if (rnd < currentNetProbability) {
                return enemyType;
            }
        }

        return enemyTypes[enemyTypes.Count - 1]; // this really should never be necessary
    }

    float GetRegionSizeMultiplier (Region region) { // difficulty should be less for smaller regions and more for larger ones. returns a value describing how many multiples of the average size this region's size is
        int regionSum = 0;
        foreach (var r in perlinGen.regions) {
            regionSum += r.area;
        }

        float averageArea = (float) regionSum / (float) perlinGen.regions.Count;
        float multiplier = (float) region.area / (float) averageArea;
        multiplier -= 1;
        multiplier *= 0.5f; // this cuts the multiplier strength in half
        multiplier += 1;
        return multiplier;
    }

    void SpawnEnemies (Region region, float difficulty) {
        int usedArea = 0; // each enemy 'uses' a part of the area larger than itself, this prevents overcrowding, while allowing enemies to spawn close together in groups


        float difficultyMultiplier = GetRegionSizeMultiplier (region); // larger difficulty for larger regions
        float remainingDifficulty = difficulty * difficultyMultiplier;
        var remainingTriesStart = 50;
        var remainingTries = remainingTriesStart; // just in case anything goes wrong (room too small or something) dont enter an infinite loop
        while (remainingDifficulty > 0 && remainingTries > 0 && usedArea < region.area) {
            var enemyType = RandomEnemyType ();
            if (Random.Range(0f, 1f) > 0.5f) { // 0.5 chance to spawn a group
                int maxEnemies = enemyType.UseArea != 0 ? (region.area - usedArea) / enemyType.UseArea : 999999;
                var enemiesInGroup = SpawnEnemyGroup (region, enemyType, remainingDifficulty, maxEnemies);
                usedArea += enemiesInGroup * enemyType.UseArea;
                remainingDifficulty -= enemiesInGroup * enemyType.difficulty;
            } else { // 0.5 chance to spawn single enemy
                if (SpawnSingleEnemy(region, enemyType)) {
                    remainingDifficulty -= enemyType.difficulty;
                    usedArea += enemyType.UseArea;
                }
            }
            
            remainingTries -= 1;
        }
    }

    bool SpawnSingleEnemy (Region region, EnemyType enemyType) {
        var regionSpaces = new List<Vector2Int> (region.cells);
        while (regionSpaces.Count > 0) {
            int index = Random.Range (0, regionSpaces.Count);
            var spawnPos = regionSpaces[index];
            if (CheckAreaEmpty(spawnPos, enemyType.GridRadius, region)) {
                var obj = Instantiate (enemyType.prefab, GridManager.Instance.GridToWorldPos (spawnPos), Quaternion.identity, transform) as GameObject;
                if (enemyType.rotateSprite == true) {
                   obj.GetComponent<Rigidbody2D> ().MoveRotation (Random.Range (0, 360f));
                }    
                AddToEnemiesPos (spawnPos, enemyType.GridRadius);
                return true;
            }
            regionSpaces.RemoveAt (index);
        }

        return false;
    }

    /// returns total difficulty of spawned group
    int SpawnEnemyGroup (Region region, EnemyType enemyType, float maxDifficulty, int maxEnemies) {
        float totalDifficulty = 0;
        int enemiesSpawned = 0;

        int groupSize = GenerateGroupSize (maxDifficulty, enemyType.difficulty, enemyType.groupSizeReturnChance);
        Vector2Int groupCentre = region.cells.ElementAt (Random.Range(0, region.cells.Count));
        //float groupDensity = GenerateGroupSize (10, 1, 0.2f); // the function can be used for many things
        float rnd = Random.Range(0f, 1f);
        int groupRadiusCenter = Mathf.RoundToInt (0.5f * Mathf.Sqrt(groupSize) * Mathf.Pow(rnd, 2)); // just a random function, with a density multiplier
        int groupRadiusVariance = Random.Range (0, Mathf.Max(1, Mathf.RoundToInt (groupRadiusCenter * 0.5f)) + 1);
        for (int i = 0; i < groupSize; i++) {
            if (enemiesSpawned >= maxEnemies) break;

            var minRadius = Mathf.Max (0, groupRadiusCenter - groupRadiusVariance);
            var maxRadius = Mathf.Max (minRadius, groupRadiusCenter + groupRadiusVariance);
            var rotationalVariance = rnd;
            if (SpawnEnemyNearLocation (groupCentre, region, minRadius, maxRadius, enemyType, rotationalVariance)) {
                totalDifficulty += enemyType.difficulty;
            }

            enemiesSpawned++;
        }

        return enemiesSpawned;
    }

    /// keeps adding to the member number with a set probability of finishing and returning the current member number. Probability curve is (1/(1/1-p)^x).
    /// Will never exceed the difficulty limit for combined member difficulty.
    /// chanceToReturn = 0.2 gives 10% chance to get over 10, and 0.7% to get over 20.
    int GenerateGroupSize (float maxDifficulty, float memberDifficulty, float chanceToReturn) {
        if (memberDifficulty <= maxDifficulty) return 1;  // 
        
        var remainingDifficulty = maxDifficulty;
        int currentMembers = 2;
        remainingDifficulty -= memberDifficulty * 2; // we just added 2 members, so add 2 difficulty
        while (remainingDifficulty > 0) {
            if (Random.Range (0f, 1f) < chanceToReturn) {
                return currentMembers;
            }

            currentMembers += 1;
            remainingDifficulty -= memberDifficulty;
        }

        return currentMembers;
    }

    /// returns false if it cant find a location, true otherwise
    bool SpawnEnemyNearLocation (Vector2Int pos, Region region, int minDistance, int maxDistance, EnemyType enemyType, float rotationalPercentVariance) {
        var finalPositions = new List<Vector2Int> (); // contains one empty position from each square ring, if those positions exist
        
        int distance = minDistance;
        while (distance <= maxDistance) {
            var positions = GetSquareRing (pos, distance);
            while (positions.Count > 0) {
                int posIndex = Random.Range(0, positions.Count);
                var newPos = positions[posIndex];
                if (CheckAreaEmpty(newPos, enemyType.GridRadius, region)) {
                    finalPositions.Add (newPos);
                    break;
                }
                positions.RemoveAt (posIndex);
            }

            distance += 1;
        }

        if (finalPositions.Count == 0) return false;

        var spawnPos = finalPositions[Random.Range (0, finalPositions.Count)];
        AddToEnemiesPos (spawnPos, enemyType.GridRadius);
        
        var inwardDirection = (Vector2) pos - (Vector2) spawnPos;
        Quaternion rotation = Quaternion.LookRotation(Vector3.forward, inwardDirection);

        var obj = Instantiate (enemyType.prefab, GridManager.Instance.GridToWorldPos (spawnPos), rotation, transform) as GameObject;
        if (enemyType.rotateSprite == true) {
            float rotateAngle = Random.Range (-rotationalPercentVariance * 180, rotationalPercentVariance * 180);
            obj.GetComponent<Rigidbody2D> ().MoveRotation (rotateAngle);
        }

        return true;
    }

    /// use 0 radius for a single cell enemy
    void AddToEnemiesPos (Vector2Int pos, int radius) {
        if (radius == 0) {
            enemiesPos.Add (pos);
            return;
        }

        for (int y = pos.y - radius; y < pos.y + radius + 1; y++) {
            for (int x = pos.x - radius; x < pos.x + radius + 1; x++) {
                var addPos = new Vector2Int (x, y);
                enemiesPos.Add (addPos);
            }
        }
    }

    /// use 0 radius for single cell
    bool CheckAreaEmpty (Vector2Int gridPos, int radius, Region region) {
        if (radius == 0) {
            if (!enemiesPos.Contains(gridPos) && region.cells.Contains(gridPos) && GridManager.Instance.CheckInGridBounds(gridPos)) {
                return true;
            } else {
                return false;
            }
        }

        for (int y = gridPos.y - radius; y < gridPos.y + radius + 1; y++) {
            for (int x = gridPos.x - radius; x < gridPos.x + radius + 1; x++) {
                var checkPos = new Vector2Int (x, y);
                if (enemiesPos.Contains(gridPos) || !region.cells.Contains(gridPos) || !GridManager.Instance.CheckInGridBounds(gridPos) || GridManager.Instance.GetCell (gridPos) == CellType.Wall) {
                    return false;
                }
            }
        }
        return true;
    }

    List<Vector2Int> GetSquareRing (Vector2Int center, int radius) {
        if (radius == 0) {
            return new List<Vector2Int> () {center};
        }
        var returnList = new List<Vector2Int> ();
        for (int i = 0; i < radius * 2 + 1; i++) { // top
            returnList.Add (new Vector2Int (center.x + i - radius, center.y + radius));
        }

        for (int i = 0; i < radius * 2 + 1; i++) { // bottom
            returnList.Add (new Vector2Int (center.x + i - radius, center.y - radius));
        }

        for (int i = 1; i < radius * 2 - 1; i++) { // left
            returnList.Add (new Vector2Int (center.x - radius, center.y - radius + i));
        }

        for (int i = 1; i < radius * 2 - 1; i++) { // right
            returnList.Add (new Vector2Int (center.x + radius, center.y - radius + i));
        }

        return returnList;
    }

    GameObject SpawnAtGridPos (GameObject prefab, Vector2Int gridPos) {
        Vector2 spawnPos = GridManager.Instance.GridToWorldPos(gridPos);
        return Instantiate (prefab, spawnPos, prefab.transform.rotation, transform) as GameObject;
    }

    void ResetVariables () {
        enemiesPos.Clear ();
    }
}
