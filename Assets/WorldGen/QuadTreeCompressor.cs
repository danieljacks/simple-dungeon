﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuadTreeCompressor {

    public CellType[,] walls;
    public float wallSize = 1.0f;

    public List<RectInt> wallRects = new List<RectInt> ();

    public Dictionary<Vector2Int, bool> wallsUsed = new Dictionary<Vector2Int, bool> (); // dictionary of (position, used in a block rect yet)


    public QuadTreeCompressor (CellType[,] _walls, float _wallSize) {
        walls = _walls;
        wallSize = _wallSize;
    }

    public void Compress () {
        CompressRegion (new Vector2Int (0, 0), new Vector2Int (walls.GetLength(0) - 1, walls.GetLength(1) - 1)); // start recursive function on full region
    }

    // notes and diagrams on paper
    private void CompressRegion (Vector2Int botLeft, Vector2Int topRight) {
        int w = topRight.x - botLeft.x + 1; // width of the region in number of cells. + 1 because indices start at 0, meaning its needed to just be even and balance that
        int h = topRight.y - botLeft.y + 1; // height as number of cells

        CellType firstCell = walls[botLeft.x, botLeft.y];
        
        bool homogenous = true; // all cells in region of same cell type
        if (w <= 1 || h <= 1) {
            if (firstCell == CellType.Wall) {
                wallRects.Add (new RectInt (botLeft.x, botLeft.y, w, h)); // nothing to subdivide
            }
            return;
        }

        // check if all blocks are the same
        for (int y = botLeft.y; y < topRight.y + 1; y++) {
            for (int x = botLeft.x; x < topRight.x + 1; x++) {
                if (walls[x, y] != firstCell) {
                    homogenous = false;
                    break;
                }
            }
            if (homogenous == false) {
                break;
            }
        }

        if (homogenous == true) {
            if (firstCell == CellType.Wall) {
                wallRects.Add (new RectInt (botLeft.x, botLeft.y, w, h)); // the adde (1,1) is needed because if we want just one wall block, botLeft = topRight, but the region size is really (1,1) not (0,0)
            }
        } else {
            // compress the next 4 regions
            Vector2Int midpoint = botLeft + new Vector2Int (w / 2, h / 2) - new Vector2Int (1, 1); // Vector2Int values refer to the bottom left of a region, hence the -(1,1)
            CompressRegion (botLeft, midpoint); // bot left region
            CompressRegion (new Vector2Int (botLeft.x, midpoint.y + 1), new Vector2Int (midpoint.x, topRight.y)); // top left
            CompressRegion (new Vector2Int (midpoint.x + 1, botLeft.y), new Vector2Int (topRight.x, midpoint.y)); // bot right
            CompressRegion (midpoint + new Vector2Int (1, 1), topRight); // top right
        }
    }
}
