﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CatchCo;

public class GridManager : Singleton<GridManager> {

	public int gridWidth = 10;
	public int gridHeight = 10;
	public float cellSize = 1.0f;

	/// bottom left is 0,0 and coordinates are positive.
	private CellType[,] _grid;
	public CellType[,] grid {
		get {
			if (_grid == null) _grid = new CellType[gridWidth, gridHeight];
			return _grid;
		}
		set {
			_grid = value;
		}
	}

	void Start () {


		//grid = grid ?? new CellType[gridWidth, gridHeight];
		//gridObjects = gridObjects ?? new GameObject[gridWidth, gridHeight];

		//FillAll (CellType.Wall);
	}

	[ExposeMethodInEditor]
	public void ResetGrid () {
		foreach (Transform child in transform.GetComponentsInChildren<Transform>()) {
			if (child == transform) continue;
			if (Application.isPlaying) {
				Destroy (child.gameObject);
			} else {
				DestroyImmediate (child.gameObject);
			}
		}

		FillAll (CellType.Empty);

		_grid = new CellType[gridWidth, gridHeight];

		print ("done reset");
	}

	public void FillAll (CellType cellType) {
		_grid = _grid ?? new CellType[gridWidth, gridHeight];
		for (int y = 0; y < _grid.GetLength (1); y++) {
			for (int x = 0; x < _grid.GetLength (0); x++) {
				SetCell (new Vector2Int (x, y), cellType);
			}
		}
	}

	public void SetCell (Vector2Int pos, CellType type) {
		grid[pos.x, pos.y] = type;
	}
	public void SetCell (Vector2 worldPos, CellType type) {
		SetCell (WorldToGridPos (worldPos), type);
	}

	public CellType GetCell (Vector2Int pos) {
		if (!CheckInGridBounds(pos)) return CellType.Empty;
		return grid[pos.x, pos.y];
	}
	public CellType GetCell (Vector2 pos) {
		return GetCell (WorldToGridPos(pos));
	}

	public Vector2 GridToWorldPos (Vector2Int gridPos) {
		Vector2 worldPos = new Vector2 (gridPos.x - (gridWidth / 2), gridPos.y - (gridHeight / 2));
		worldPos *= cellSize;
		return worldPos;
	}

	public Vector2Int WorldToGridPos (Vector2 worldPos) {
		int x = Mathf.RoundToInt((worldPos.x / cellSize) + (gridWidth / 2));
		int y = Mathf.RoundToInt((worldPos.y / cellSize) + (gridHeight / 2));
		Vector2Int gridPos = new Vector2Int (x, y);

		return gridPos;
	}

	/// Returns true if the point is inside the grid
	public bool CheckInGridBounds (Vector2Int gridPos) {
		if (	gridPos.x == Mathf.Clamp(gridPos.x, 0, gridWidth - 1) 
			&& 	gridPos.y == Mathf.Clamp(gridPos.y, 0, gridHeight - 1)) {
				return true;
			} else {
				return false;
			}
	}
	public bool CheckInGridBounds (Vector2 worldPos) {
		return CheckInGridBounds (WorldToGridPos (worldPos));
	}
}
