﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CatchCo;

public class Agent_realtime : MonoBehaviour {

	public Agent agent;
	public GameObject markerPrefab;
	private Transform markerT;

	void Start () {
		agent = agent ?? new Agent ();
		markerT = Instantiate (markerPrefab, transform.position, Quaternion.identity).transform;
	}

	void Update () {
		agent.Tick ();
		transform.position = agent.currentPosition;
		markerT.position = agent.idealPosition;
	}

	[ExposeMethodInEditor]
	void Restart () {
		agent.Restart ();
		GetComponentInChildren<TrailRenderer>().Clear ();
	}
}
