﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CatchCo;

public class WGenPerlin : MonoBehaviour {
    public FastNoise noise;
    [Range(0f, 1.0f)]
    public float wallCutoff; /// anything lower than this will be a wall
    public float frequency = 1.0f;
    public int seed = 1337;
    public bool trimCorners = true;
    public bool visualizeRegions = false;
    public bool deleteEdgeRegions = true;
    public bool deleteSmallRegions = true;
    public int minRegionArea = 15;
    public bool smoothEdges = true;
    public float smoothEdgesMin = 0f; // this number is chosen randomly between the bounds for each region separately. The integer part determines the number of passes at 100% certainty of smoothing, the decimal is a final pass with a % chance of smoothing any given block
    public float smoothEdgesMax = 2f;
    public bool generatePassages = true;
    public int minPassageWidth = 1;
    public int maxPassageWidth = 2;
    public GameObject marker;
    [Range(0f, 1.0f)]
    public float markerAlpha = 0.5f;
    
    private List<Vector2Int> directions = new List<Vector2Int> () {Vector2Int.right, Vector2Int.up, Vector2Int.left, Vector2Int.down};
    public List<Region> regions = new List<Region> ();

    [ExposeMethodInEditor]
    public void RandomizeSeed () {
        seed = Random.Range(1, 99999);
    }

    [ExposeMethodInEditor]
    public void GeneratePerlin () {
        GridObjectPlacer.Instance.ClearObjects ();
        GridManager.Instance.ResetGrid ();

        var oldState = Random.state;
        Random.InitState (seed);

        noise = new FastNoise ();
        noise.SetNoiseType (FastNoise.NoiseType.PerlinFractal);
        noise.SetFrequency (frequency);
        noise.SetSeed (seed);

        float smallestValue = Mathf.Infinity;
        float largestValue = Mathf.NegativeInfinity;

        int width = GridManager.Instance.gridWidth;
        int height = GridManager.Instance.gridHeight;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                float val = noise.GetNoise (x * GridManager.Instance.cellSize, y * GridManager.Instance.cellSize);
                val += 0.5f; // 0..1 range

                var fadeEdgePercent = 0.2f; // percent that is faded around the edges
                float normalizedX = (float) x / (float) width;
                normalizedX = normalizedX * 2 - 1f; // -1..1 range
                float normalizedY = (float) y / (float) height;
                normalizedY = normalizedY * 2 - 1f; // -1..1 range
                var fadeEdgeFactor = Mathf.Min (1, (1/fadeEdgePercent) - Mathf.Max (Mathf.Abs(normalizedX), Mathf.Abs(normalizedY))*(1/fadeEdgePercent)); // should work, as ive tested on wolfram alpha. Function looks sort of like an inverted square flat topped bowl

                val *= fadeEdgeFactor;

                if (val < wallCutoff) {
                    GridManager.Instance.SetCell (new Vector2Int (x, y), CellType.Wall);
                } else {
                    GridManager.Instance.SetCell (new Vector2Int(x, y), CellType.Empty);
                }

                if (val > largestValue) largestValue = val;
                if (val < smallestValue) smallestValue = val;

            }
        }

        GenerateRegions ();
        if (visualizeRegions) {
            VisualizeRegions ();
        }

        if (trimCorners == true) {
            FillCorners ();
        }
        
        if (deleteSmallRegions == true || deleteEdgeRegions == true) {
            FixRegions ();
        }

        if (smoothEdges == true) {
            SmoothEdges ();
        }

        if (generatePassages == true) {
            GeneratePassages ();
        }

        Random.state = oldState;
        print ("done perlin");

        GridObjectPlacer.Instance.GenerateObjects ();
    }

    void GeneratePassages () {
        // basically a poorly written implementation of a traveling salesman algorithm (https://codereview.stackexchange.com/questions/67823/simple-attempt-to-solve-the-travelling-salesman-prob)
        var regionConnections = new List<Region> ();
        var remainingRegions = new List<Region> (regions);

        while (remainingRegions.Count > 0) {
            var regionToAdd = remainingRegions[Random.Range (0, remainingRegions.Count)];
            int insertIndex = 0;

            if (regionConnections.Count < 2) {
                regionConnections.Insert (0, regionToAdd);
                remainingRegions.Remove (regionToAdd);
                continue;
            } 
            
            // get closest region 
            int closestIndex = 0;
            float closestDistance = Mathf.Infinity;
            foreach (var region in regionConnections) {
                var distance = Vector2.SqrMagnitude (regionToAdd.GetCenterPos () - region.GetCenterPos ());
                if (distance < closestDistance) {
                    closestDistance = distance;
                    closestIndex    = regionConnections.IndexOf (region);
                }
            }

            if (closestIndex == 0) { // if beginning of end, there is only one spot to connect to
                insertIndex = 1;
            } else if (closestIndex == regionConnections.Count - 1) {
                insertIndex = closestIndex - 1;
            } else { // choose connection side based on which side is closer
                var prevRegionDistance = Vector2.SqrMagnitude (regionToAdd.GetCenterPos() - regionConnections[closestIndex - 1].GetCenterPos ());
                var nextRegionDistance = Vector2.SqrMagnitude (regionToAdd.GetCenterPos() - regionConnections[closestIndex + 1].GetCenterPos ());
                
                if (prevRegionDistance < nextRegionDistance) {
                    insertIndex = closestIndex - 1;
                } else {
                    insertIndex = closestIndex + 1;
                }
            }

            regionConnections.Insert (closestIndex, regionToAdd);
            remainingRegions.Remove (regionToAdd);
        }

        //regions = remainingRegions;

        for (int i = 0; i < regionConnections.Count - 1; i++) { // -1 because we dont want to generate a passage from the last region, only to the last region
            var passageGen = GetComponent<WGenPassage> ();
            passageGen.startPoint = GridManager.Instance.GridToWorldPos (regionConnections[i].GetCenterPos ());
            passageGen.endPoint = GridManager.Instance.GridToWorldPos (regionConnections[i + 1].GetCenterPos ());
            passageGen.noiseOffset = Random.Range (0f, 99999f);

            passageGen.Regenerate ();
        }
    }

    void SmoothEdges () {
        foreach (var region in regions) {
            float smoothAmount = Random.Range (smoothEdgesMin, smoothEdgesMax);

            int fullPasses = (int) smoothAmount;
            float smoothChance = smoothAmount - fullPasses;

            var addToRegion = new List<Vector2Int> ();

            // full passes
            for (int i = 0; i < fullPasses; i++) {
                foreach (var pos in region.cells) {
                    foreach (var direction in directions) {
                        SmoothWall (pos + direction, addToRegion);
                    }
                }

                foreach (var pos in addToRegion) {
                    region.cells.Add (pos);
                }
            }

            // partial pass
            float t = 1f;
            foreach (var pos in region.cells) {
                foreach (var direction in directions) {
                    if (GridManager.Instance.GetCell (pos + direction) == CellType.Wall) { // only do something if a wall is adjacent to the region position (which is empty space)
                        if (t >= 1) {
                            SmoothWall (pos + direction, addToRegion);
                            t -= 1;
                        }
                        t += smoothChance;
                    }
                }
            }

            foreach (var pos in addToRegion) {
                region.cells.Add (pos);
            }
        }
    }

    void SmoothWall (Vector2Int pos, List<Vector2Int> addToRegion) {
        int adjacentWalls = 0;

        foreach (var direction in directions) {
            if (GridManager.Instance.GetCell (pos + direction) == CellType.Wall) {
                adjacentWalls += 1;
            }
        }

        if (GridManager.Instance.CheckInGridBounds (pos) && GridManager.Instance.GetCell (pos) == CellType.Wall && adjacentWalls <= 2) {
            GridManager.Instance.SetCell (pos, CellType.Empty);
            addToRegion.Add (pos);
        }
    }

    void FillCorners () {
        for (int y = 0; y < GridManager.Instance.gridHeight; y++) {
            for (int x = 0; x < GridManager.Instance.gridWidth; x++) {
                RecursiveFillCorners (new Vector2Int (x, y));
            }
        }
    }

    void RecursiveFillCorners (Vector2Int pos) {
        if (GridManager.Instance.GetCell(pos) == CellType.Wall) return;

        // check how many empty spaces are adjacent
        int adjacentEmpty = 0;
        
        foreach (var direction in directions) {
            var checkPos = pos + direction;
            if (GridManager.Instance.GetCell(checkPos) == CellType.Empty) {
                adjacentEmpty += 1;
            }
        }

        // fill this and call on adjacent squares if there is not enough adjacent open space
        if (adjacentEmpty <= 1) {
            GridManager.Instance.SetCell (pos, CellType.Wall);

            foreach (var direction in directions) {
                RecursiveFillCorners (pos + direction);
            }
        }
    }

    void FixRegions () {
        var regionsToDelete = new List<Region> ();

        if (deleteEdgeRegions) {
            foreach (var region in regions) {
                if (region.IsEdgeRegion () == true) {
                    regionsToDelete.Add (region);
                }
            }
        }

        if (deleteSmallRegions) {
            foreach (var region in regions) {
                if (regionsToDelete.Contains (region)) continue;

                var bounds = region.GetBounds ();
                int boundsArea = bounds.width * bounds.height;

                int minArea = Mathf.FloorToInt (minRegionArea / GridManager.Instance.cellSize);

                if (region.area < minArea) {
                    regionsToDelete.Add (region);
                }
            }
        }

        // delete selected regions
        foreach (var region in regionsToDelete) {
            foreach (var pos in region.cells) {
                GridManager.Instance.SetCell (pos, CellType.Wall);
            }
            regions.Remove (region);
        }
    }

    void GenerateRegions () {
        regions.Clear ();

        for (int y = 0; y < GridManager.Instance.gridHeight; y++) {
            for (int x = 0; x < GridManager.Instance.gridWidth; x++) {
                var pos = new Vector2Int (x, y);
                if (!IsInSomeRegion (pos) && GridManager.Instance.GetCell (pos) == CellType.Empty) {
                    regions.Add (new Region ());
                    GenerateRegionFrom (pos, regions.Count - 1);
                }
            }
        }
    }

    void AddToRegionRecursive (Vector2Int pos, int regionIndex) {
        if (IsInSomeRegion (pos) || GridManager.Instance.GetCell (pos) == CellType.Wall) return;
        if (GridManager.Instance.CheckInGridBounds(pos) == false) return;
        
        regions[regionIndex].cells.Add (pos);

        foreach (var direction in directions) {
            AddToRegionRecursive (pos + direction, regionIndex);
        }
    }

    // basically a flood fill algorithm using a queue to avoid (by simulating) recursion, as recursion leads to stack overflow crashes
    void GenerateRegionFrom (Vector2Int startPos, int regionIndex) {
        if (IsInSomeRegion (startPos) || GridManager.Instance.GetCell (startPos) == CellType.Wall) return;
        if (GridManager.Instance.CheckInGridBounds(startPos) == false) return;

        regions[regionIndex].cells.Add (startPos);

        var queue = new Queue<Vector2Int> ();
        queue.Enqueue (startPos);
        while (queue.Count > 0) {
            var n = queue.Dequeue ();
            foreach (var direction in directions) {
                var nextPos = n + direction;
                if (!IsInSomeRegion(nextPos) && GridManager.Instance.CheckInGridBounds(nextPos) == true && GridManager.Instance.GetCell (nextPos) != CellType.Wall) {
                    queue.Enqueue (nextPos);
                    regions[regionIndex].cells.Add (nextPos);
                }
            }
        }
    }

    bool IsInSomeRegion (Vector2Int pos) {
        foreach (var region in regions) {
            if (region.cells.Contains(pos) == true) {
                return true;
            }
        }
        return false;
    }

    void VisualizeRegions () {
        var parentObject = GameObject.Find ("regionMarkerParent");
        if (parentObject == null) {
            parentObject = new GameObject ("regionMarkerParent");
        }

        foreach (Transform child in parentObject.transform) {
            if (Application.isPlaying) {
                Destroy (child.gameObject);
            } else {
                DestroyImmediate (child.gameObject);
            }
        }

        foreach (var region in regions) {
            var regionColor = Random.ColorHSV();
            regionColor.a = markerAlpha;
            foreach (var pos in region.cells) {
                var worldPos = GridManager.Instance.GridToWorldPos (pos);
                var obj = Instantiate (marker, new Vector3 (worldPos.x, worldPos.y, 0), Quaternion.identity, parentObject.transform) as GameObject;
                obj.GetComponent<SpriteRenderer> ().color = regionColor;
                obj.transform.localScale *= GridManager.Instance.cellSize;
            }
        }
    }
}
