﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CatchCo;

public class GridObjectPlacer : Singleton<GridObjectPlacer> {
    public GameObject wallPrefab;
    public bool compressWalls = true;

    private QuadTreeCompressor _compressor;

    [ExposeMethodInEditor]
    public void ClearObjects () {
        foreach (Transform obj in transform) {
            if (Application.isPlaying) {
                Destroy (obj.gameObject);
            } else {
                DestroyImmediate (obj.gameObject);
            }
        }

        print ("done clear");
    }

    [ExposeMethodInEditor]
    public void GenerateObjects () {
        ClearObjects ();
        if (compressWalls == true) {
            _compressor = new QuadTreeCompressor (GridManager.Instance.grid, GridManager.Instance.cellSize);
            _compressor.Compress ();

            foreach (RectInt wallRect in _compressor.wallRects) {
                var min = GridManager.Instance.GridToWorldPos (wallRect.min);
                var max = GridManager.Instance.GridToWorldPos (wallRect.max);
                var posRect = new Rect (min.x, min.y, max.x - min.x, max.y - min.y);
                posRect.center -= new Vector2 (1, 1) * GridManager.Instance.cellSize * 0.5f; // center should be on grid point, not bottom left corner on grid point
                var obj = Instantiate (wallPrefab, posRect.center, Quaternion.identity, transform) as GameObject;
                obj.transform.localScale = new Vector3 (posRect.size.x, posRect.size.y, 1);
            }
        } else {
            for (int y = 0; y < GridManager.Instance.gridHeight; y++) {
                for (int x = 0; x < GridManager.Instance.gridWidth; x++) {
                    var gridPos = new Vector2Int (x, y);
                    if (GridManager.Instance.GetCell (gridPos) == CellType.Wall) {
                        var obj = Instantiate (wallPrefab, GridManager.Instance.GridToWorldPos (gridPos), Quaternion.identity, transform) as GameObject;
                        obj.transform.localScale = new Vector3 (GridManager.Instance.cellSize, GridManager.Instance.cellSize, 1);
                    }
                }
            }
        }

        print ("done generate");
    }
}
