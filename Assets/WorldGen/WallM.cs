﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallM : CellM {

	public override CellType CellType {
		get {
			return CellType.Wall;
		}
	}
}
